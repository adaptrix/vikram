<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('about', function () {
    return view('about');
});

Route::get('our-team', function () {
    return view('our-team');
});

Route::get('training', function () {
    return view('training');
});

Route::get('gallery', function () {
    return view('gallery');
});




Route::get('containerised-cargo', function () {
    return view('containerised-cargo');
});
Route::get('loose-cargo', function () {
    return view('loose-cargo');
});
Route::get('abnormal-cargo', function () {
    return view('abnormal-cargo');
});
Route::get('tracking', function () {
    return view('tracking');
});

Route::get('contact-us', function () {
    return view('contact-us');
});

Route::get('csr', function () {
    return view('csr');
});



Route::get('newsevents', function () {
    return view('newsevents');
});

Route::get('qhse', function () {
    return view('qhse');
});

Route::get('news1', function () {
    return view('news1');
});

Route::get('news2', function () {
    return view('news2');
});

Route::get('news3', function () {
    return view('news3');
});

Route::get('career', function () {
    return view('career');
});

Route::post('job-application','FormController@jobApplication');
Route::post('contact-us','FormController@contactUs');
Route::post('request-quote','FormController@requestQuote');

Route::get('test', function () {
    dd(config('recaptcha.secret_key'));
});