<h1>Vikram Transport Limited</h1>
<h3>Client job application request for {{$job_title}} position</h3><br>
<br>
<br>
<p>First Name:</p>
<p><strong>{{$first_name}}</strong></p>
<p>Last Name:</p>
<p><strong>{{$last_name}}</strong></p>
<p>Email address:</p>
<p><strong>{{$email}}</strong></p>
<p>Phone number:</p>
<p><strong>{{$phone_number}}</strong></p>
<p>Location:</p>
<p><strong>{{$location}}</strong></p>
<p>Address:</p>
<p><strong>{{$address}}</strong></p> 