<h1>Vikram Transport Limited</h1>
<h3>Client Quote Request</h3><br>
<br>
<br>
<p>Contact Name:</p>
<p><strong>{{$contact_name}}</strong></p>
<p>Company Name:</p>
<p><strong>{{$company_name}}</strong></p>
<p>Email address:</p>
<p><strong>{{$email}}</strong></p>
<p>Phone number:</p>
<p><strong>{{$phone_number}}</strong></p>
<p>Location from:</p>
<p><strong>{{$location_from}}</strong></p>
<p>Location to:</p>
<p><strong>{{$location_to}}</strong></p>
<p>Cargo description:</p>
<p><strong>{{$cargo_description}}</strong></p> 