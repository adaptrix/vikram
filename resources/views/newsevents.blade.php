@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>Our feeds </h3>
                    <h2>Latest News & Events</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">news & events</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Blog & News =-=-=-=-=-=-= -->
<section id="blog" class="custom-padding">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">

                <!-- blog-grid -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="news-box">
                            <div class="news-thumb">
                              <a href="{{url('news1')}}"><img alt="" class="img-responsive" src="images/blog/1.jpg"></a>
                              {{-- <div class="date"> <strong>21</strong>
                              <span>Jun</span> </div> --}}
                            </div>
                            <div class="news-detail">
                                <h2><a title="" href="{{url('news1')}}">1450 tonnes of load from DRC TO DAR</a></h2>
                                <p>This month our contract with a mining industry got finalized, and they commenced its copper production in Kalukuluku and Chemaf. </p>
                                <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news1')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                            </div>
                            
                        </div>
                    </div>

                    <!-- blog-grid end -->

                    <!-- blog-grid -->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="news-box">
                            <div class="news-thumb">
                                <a href="{{url('news2')}}"><img alt="" class="img-responsive" src="images/blog/2.jpg"></a>
                                {{-- <div class="date"> <strong>21</strong>
                                  <span>Jun</span> </div> --}}
                            </div>
                            <div class="news-detail">
                                <h2><a title="" href="{{url('news2')}}">Zambia branch coming soon!!</a></h2>
                                <p>As a matter of continuous growth and expansion, we are very pleased to announce that the further expansion of new fleet has already been planned and we shall soon grow our presence in Zambia. </p>
                                <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news2')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                    <!-- blog-grid end -->

                    <!-- blog-grid -->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="news-box">
                            <div class="news-thumb">
                                <a href="{{url('news3')}}"><img class="img-responsive" alt="" src="images/blog/3.jpg"></a>
                                {{-- <div class="date"> <strong>21</strong>
                                  <span>Jun</span> </div> --}}
                            </div>
                            <div class="news-detail">
                                <h2><a title="" href="{{url('news3')}}">Giving back to the Community</a></h2>
                                <p>As a part of CSR Programs, The Management of VLT is on mission to host “ Health and Education” program for their employees and communities at large.</p>
                                <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news3')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                    <!-- blog-grid end -->

                <div class="clearfix"></div>


                <!-- blog-grid -->
            </div>
        </div>

        <!-- Row End -->
    </div>
    <!-- end container -->
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->

@endsection