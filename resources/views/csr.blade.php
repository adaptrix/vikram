@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-9 page-heading text-left">
                    <h3>CSR statement </h3>
                    <h2>Corporate Social Responsibility</h2>
                </div>
                <div class="col-sm-12 col-md-3 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">CSR</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= About Section =-=-=-=-=-=-= -->

<section class="padding-top-70" id="about" style="padding-bottom: 30px;">
    <div class="container">
        <div class="row clearfix">
            <!--Column-->
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="about-title">
                    <h2>CSR Statement</h2>
                    <p>Contributing to the social and economic development of the communities and directing our effort to build a sustainable way of life for the betterment of the society has always been a dream front for VLT.</p>
                    <p>Our philosophy for corporate citizenship focuses on “Healthcare and Education” to look after the wellbeing of our employees and the communities around which we operate.  It also extends to ethical conduct in our dealings with suppliers, clients and other stakeholders.</p>
                    <p>Education is a core part of our mission to help solve the society’s toughest problems. Over the years, we have been able to enroll a number of students in schools and improving their academic performance hence increasing the chances that they will advance to post-secondary education.  We believe that building students’ skills in these disciplines is the first step in developing a sustainable workforce for the digital economy.</p>
                    <p>We have also pledged to financially support the treatment for children suffering from cancer. Increasing access to medicine for these children is the basis of our day-to-day business model. It is in the hearts of the VLT team to demonstrate compassion and care for others. </p>
                </div>

            </div>

            <!-- RIght Grid Form -->
            {{-- <div class="col-md-6 col-sm-12 our-gallery col-xs-12" style="padding: 125px;">
                <a class="tt-lightbox" href="images/crew.png"> <img class="img-responsive" alt="Image" src="images/csr.jpg"></a>

            </div> --}}
        </div>
    </div>
</section>

    <!-- =-=-=-=-=-=-= Our Services =-=-=-=-=-=-= -->
    <section class="custom-padding services">
          <div class="container">
              
              <!-- Row -->
              <div class="main-boxes">
                  <div class="row owl-carousel-3">
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a data-fancybox="gallery1" href="images/sos/1.jpg"><img alt="" src="images/sos/1.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/2.jpg" style="display:none;"><img alt="" src="images/sos/2.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/3.jpg" style="display:none;"><img alt="" src="images/sos/3.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/4.jpg" style="display:none;"><img alt="" src="images/sos/4.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/5.jpg" style="display:none;"><img alt="" src="images/sos/5.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/6.jpg" style="display:none;"><img alt="" src="images/sos/6.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/7.jpg" style="display:none;"><img alt="" src="images/sos/7.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/8.jpg" style="display:none;"><img alt="" src="images/sos/8.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/9.jpg" style="display:none;"><img alt="" src="images/sos/9.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/10.jpg" style="display:none;"><img alt="" src="images/sos/10.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/11.jpg" style="display:none;"><img alt="" src="images/sos/11.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/12.jpg" style="display:none;"><img alt="" src="images/sos/12.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/13.jpg" style="display:none;"><img alt="" src="images/sos/13.jpg"></a>
                                  <a data-fancybox="gallery1" href="images/sos/14.jpg" style="display:none;"><img alt="" src="images/sos/14.jpg"></a>
                              </div>
                              <div class="services-text">
                                  <h4>SOS</h4>
                                
                              </div>
                              
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->
  
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/1.jpg"><img alt="" src="images/hiyariorphanage/1.jpg"></a>
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/2.jpg" style="display:none;"><img alt="" src="images/hiyariorphanage/2.jpg"></a>
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/3.jpg" style="display:none;"><img alt="" src="images/hiyariorphanage/3.jpg"></a>
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/4.jpg" style="display:none;"><img alt="" src="images/hiyariorphanage/4.jpg"></a>
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/5.jpg" style="display:none;"><img alt="" src="images/hiyariorphanage/5.jpg"></a>
                                  <a data-fancybox="gallery2" href="images/hiyariorphanage/6.jpg" style="display:none;"><img alt="" src="images/hiyariorphanage/6.jpg"></a>
                              </div>
                              <div class="services-text">
                                  <h4>HIYARI ORPHANAGE</h4>
                                  
                              </div><br>
                              
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->
  
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a data-fancybox="gallery3" href="images/almadina/1.jpg"><img alt="" src="images/almadina/1.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/2.jpg" style="display:none;"><img alt="" src="images/almadina/2.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/3.jpg" style="display:none;"><img alt="" src="images/almadina/3.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/4.jpg" style="display:none;"><img alt="" src="images/almadina/4.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/5.jpg" style="display:none;"><img alt="" src="images/almadina/5.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/6.jpg" style="display:none;"><img alt="" src="images/almadina/6.jpg"></a>
                                  <a data-fancybox="gallery3" href="images/almadina/7.jpg" style="display:none;"><img alt="" src="images/almadina/7.jpg"></a>
                                  
                              </div>
                              <div class="services-text">
                                  <h4>AL MADINA</h4>
                                 
                              </div><br>
                              
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->

                    <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="services-grid-1">
                            <div class="service-image">
                                
                                <a data-fancybox="gallery4" href="images/maunga/1.jpg"><img alt="" src="images/maunga/1.jpg"></a>
                                <a data-fancybox="gallery4" href="images/maunga/2.jpg" style="display:none;"><img alt="" src="images/maunga/2.jpg"></a>
                                <a data-fancybox="gallery4" href="images/maunga/3.jpg" style="display:none;"><img alt="" src="images/maunga/3.jpg"></a>
                                <a data-fancybox="gallery4" href="images/maunga/4.jpg" style="display:none;"><img alt="" src="images/maunga/4.jpg"></a>
                                <a data-fancybox="gallery4" href="images/maunga/5.jpg" style="display:none;"><img alt="" src="images/maunga/5.jpg"></a>
                                <a data-fancybox="gallery4" href="images/maunga/6.jpg" style="display:none;"><img alt="" src="images/maunga/6.jpg"></a>
                                
                            </div>
                            <div class="services-text">
                                <h4>MAUNGA</h4>
                                
                                
                            </div><br>
                            
                        </div>
                    <!-- end services-grid-1 -->
                     </div>

                     <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="services-grid-1">
                            <div class="service-image">
                                <a data-fancybox="gallery5" href="images/malaikakids/1.jpg"><img alt="" src="images/malaikakids/1.jpg"></a>
                                <a data-fancybox="gallery5" href="images/malaikakids/2.jpg" style="display:none;"><img alt="" src="images/malaikakids/2.jpg"></a>
                                <a data-fancybox="gallery5" href="images/malaikakids/3.jpg" style="display:none;"><img alt="" src="images/malaikakids/3.jpg"></a>
                            </div>
                            <div class="services-text">
                                <h4>MALAIKA KIDA</h4>
                                
                                
                            </div><br>
                            
                        </div>
                    <!-- end services-grid-1 -->
                    </div>
                    <!-- end col-sm-4 -->
  
                  </div>
              </div>
              <!-- Row End -->
          </div>
          <!-- end container -->
      </section>
      <!-- =-=-=-=-=-=-= Our Services-end =-=-=-=-=-=-= -->
  

    <!-- =-=-=-=-=-=-= About End =-=-=-=-=-=-= -->

@endsection