<!DOCTYPE html>
<html lang="en">

{{-- header --}}
@include('layouts.header')
{{-- end header --}}

@yield('content')
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
    @include('layouts.footer')
    {{-- foot end --}}
	
	
</body>
</html>