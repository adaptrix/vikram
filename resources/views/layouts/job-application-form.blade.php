<div class="modal fade " id="{{$job_application_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                <div class="quotation-box-1">
                        <h2>APPLY FOR THIS POSITION</h2>
                        <div class="desc-text">
                            <p>* Required fields</p>
                        </div>
                        <form action="{{url('job-application')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <input class="form-control" type="text" placeholder="Job title*" value="{{$job_title}}" name="job_title" disabled>
                                    </div>

                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control" type="text" placeholder="First Name*" value="" name="first_name">
                                    </div>
    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input class="form-control" type="text" placeholder="Last Name*" value="" name="last_name">
                                    </div>
  
                                  <!--Form Group-->
                                  <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                      <input class="form-control" type="text" placeholder="Email Address*" value="" name="email">
                                  </div>
  
                                  <!--Form Group-->
                                  <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                      <input class="form-control" type="text" placeholder="Phone Number" value="" name="phone_number">
                                  </div>
    
                                  <!--Form Group-->
                                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                      <input class="form-control" type="text" placeholder="Location*" value="" name="location">
                                  </div> 

                                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                        <input class="form-control" type="text" placeholder="Address*" value="" name="address">
                                    </div> 
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                        <input type="file" class="form-control" value="" name="file_attachments[]" multiple="multilple">                                         
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                        <div class="g-recaptcha" data-sitekey="{{config('recaptcha.site_key')}}"></div>
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-rig ht">
                                         <button class="custom-button light">Submit Request</button> 
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>