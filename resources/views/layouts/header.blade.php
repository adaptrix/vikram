<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ScriptsBundle">
    <title>Vikram Logistics Tanzania Limited</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="images/logo.png" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/et-line-fonts.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.style.css')}}">
    <!-- =-=-=-=-=-=-= Google Fonts =-=-=-=-=-=-= -->
    <link
        href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic"
        rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
        rel="stylesheet" type="text/css">
    <!-- =-=-=-=-=-=-= Flat Icon =-=-=-=-=-=-= -->
    <link href="{{asset('css/flaticon.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Magnific PopUP CSS =-=-=-=-=-=-= -->
    <link href="{{asset('js/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- Theme Color -->
    <link rel="stylesheet" id="color" href="{{asset('css/colors/defualt.css')}}">
    <!-- For Style Switcher -->
    <link rel="stylesheet" id="theme-color" type="text/css" href="#" />
    <!-- Revolution Slider 5.x CSS settings -->
    <!-- For This Page Only -->
    <link href="{{asset('js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css" />
    <!-- Animation Css -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <!-- Menu Hover -->
    <link href="{{asset('css/bootstrap-dropdownhover.min.css')}}" rel="stylesheet">
    <!-- JavaScripts -->
    <script src="{{asset('js/modernizr.js')}}"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css" />
    <!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        ol {
            counter-reset: li;
            /* Initiate a counter */
            list-style: none;
            /* Remove default numbering */
            *list-style: decimal;
            /* Keep using default numbering for IE6/7 */
            font: 15px 'trebuchet MS', 'lucida sans';
            padding: 0;
            /* margin-bottom: 4em; */
            text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
        }

        ol ol {
            margin: 0 0 0 2em;
            /* Add some left margin for inner lists */
        }

        .rectangle-list a {
            position: relative;
            display: block;
            padding: .4em .4em .4em .8em;
            *padding: .4em;
            margin: .5em 0 .5em 2.5em;
            background: #ddd;
            color: #444;
            text-decoration: none;
            transition: all .3s ease-out;
        }

        .rectangle-list a:hover {
            background: #eee;
        }

        .rectangle-list a:before {
            content: "*";
            counter-increment: li;
            position: absolute;
            left: -2.5em;
            top: 50%;
            margin-top: -1em;
            background: rgba(1, 109, 182, 0.75);
            height: 2em;
            width: 2em;
            line-height: 2em;
            text-align: center;
            font-weight: bold;
            color: white;
        }

        .rectangle-list a:after {
            position: absolute;
            content: '';
            border: .5em solid transparent;
            left: -1em;
            top: 50%;
            margin-top: -.5em;
            transition: all .3s ease-out;
        }

        .rectangle-list a:hover:after {
            left: -.5em;
            border-left-color: rgba(1, 109, 182, 0.75);
        }
    </style>
</head>

<body>
    <!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
    <section class="top-bar">
        <div class="container">
            <div class="left-text nav-left pull-left">
                <p><span>Opening Hours :</span> Monday to Saturday - 8am to 5pm</p>
            </div>
            <!-- /.left-text -->
            <ul class="nav-right pull-right list-unstyled">
                <li> <a href="#"><i class="fa fa-lock"></i> Customer Login </a></li>
                <li> <a href="#"><i class="fa fa-lock"></i> Employee Login </a></li>
                {{-- <li> <a href="sign-up.html"><i class="fa fa-user"></i> Sign Up </a></li>
                 <li class="dropdown nav-profile"> 
                    <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">
                     <img class="img-circle resize" alt="" src="images/avatar.jpg">
                     <span class="hidden-xs small-padding"> <span>Umair</span> <i class="fa fa-caret-down"></i></span>
                    </a>
                    <ul class="dropdown-menu with-arrow pull-right">
                      <li> <a href="profile.html"> <i class="fa fa-user"></i> <span>My Profile</span> </a> </li>
                      <li> <a href="history.html"> <i class="fa fa-check"></i> <span>Tracking History</span> </a> </li>
                      <li> <a href="javascript:void(0)"> <i class="fa fa-lock"></i> <span>Payment Setting</span> </a> </li>
                      <li> <a href="javascript:void(0)"> <i class="fa fa-sign-out"></i> <span>Log Out</span> </a> </li>
                    </ul>
                </li> --}}
            </ul>
        </div>
    </section>
    {{-- <header class="header-area">
    
            
            <div class="logo-bar">
                <div class="container clearfix">
                   
                    <div class="logo">
                        <a href="{{url('/')}}"><img src="images/logo.png" alt=""></a>
    </div>


    <div class="information-content">

        <div class="info-box  hidden-sm">
            <div class="icon"><span class="icon-envelope"></span></div>
            <div class="text">EMAIL</div>
            <a href="mailt:md@vikramlogistics.co.tz">md@vikramlogistics.co.tz</a>
        </div>

        <div class="info-box">
            <div class="icon"><span class="icon-phone"></span></div>
            <div class="text">Call Now</div>
            <a class="location" href="#">+255 715 439 843</a>
        </div>
        <div class="info-box">
            <div class="icon"><span class="icon-map-pin"></span></div>
            <div class="text">Find Us</div>
            <a class="location" href="#">Jamhuri Street, Urban Rose Hotel
            </a>
        </div>
    </div>
    </div>
    </div>



    <div class="navigation-2">
        <nav class="navbar navbar-default ">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#main-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-navigation">
                    <ul class="nav navbar-nav">
                        <li class="hidden-sm {{Request::is('/')?'active':''}}"><a href={{url('/')}}>Home</a></li>

                        <li
                            class="dropdown {{Request::is('about')||Request::is('our-team')||Request::is('gallery')?'active':''}}">
                            <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"
                                data-animations="fadeInUp">About Us <span class="fa fa-angle-down"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('about')}}">About Company </a> </li>
                                <li><a href="{{url('our-team')}}">Our Team</a> </li>
                                <li><a href="{{url('gallery')}}">Gallery</a> </li>
                            </ul>
                        </li>
                        <li class="dropdown 
                                {{Request::is('containerised-cargo')||Request::is('loose-cargo')||
                                Request::is('abnormal-cargo')||Request::is('tracking')?'active':''}}"> <a
                                class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"
                                data-animations="fadeInUp">Services <span class="fa fa-angle-down"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('containerised-cargo')}}">Containerised Cargo</a> </li>
                                <li><a href="{{url('loose-cargo')}}">Loose Cargo</a> </li>
                                <li><a href="{{url('abnormal-cargo')}}">Abnormal Cargo</a> </li>
                                <li><a href="{{url('tracking')}}">Tracking</a> </li>
                            </ul>
                        </li>

                        <li class="hidden-sm {{Request::is('career')?'active':''}}"><a
                                href="{{url('career')}}">Careers</a></li>
                        <li class="hidden-sm {{Request::is('csr')?'active':''}}"><a href="{{url('csr')}}">CSR
                                Policies</a></li>
                        <li class="hidden-sm {{Request::is('qhse')?'active':''}}"><a href="{{url('qhse')}}">QHSE</a>
                        </li>
                        <li class="hidden-sm {{Request::is('contact-us')?'active':''}}"><a
                                href="{{url('contact-us')}}">Contact Us</a></li>




                    </ul>
                    <div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs"
                        style="position: unset;">
                        <a href="javascript:void(0)" class="btn btn-primary pull-right">request quote</a>
                    </div>

                </div>
        </nav>
    </div>
    </header> --}}

    <header class="header-area">
        <div class="navigation">
            <!-- navigation-start -->
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header" style="margin: -25px;">
                        <button aria-expanded="false" data-target="#main-navigation" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{url('/')}}"><img alt="" src="images/logo.png"
                                class="img-responsive"
                                style="width: 184px;margin-bottom: 0px;margin-top: 3px;margin-left: 23px;"> </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="main-navigation" class="collapse navbar-collapse" style="padding-top: 5px;">
                            <ul class="nav navbar-nav">
                                    <li class="hidden-sm {{Request::is('/')?'active':''}}"><a href={{url('/')}}>Home</a></li>
                                    
                                    <li class="dropdown {{Request::is('about')||Request::is('our-team')||Request::is('csr')||Request::is('gallery')?'active':''}}"><a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">About Us <span class="fa fa-angle-down"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{url('about')}}">About Company </a> </li>
                                            <li><a href="{{url('our-team')}}">Our Team</a> </li>
                                            <li><a href="{{url('training')}}">Training & Development</a> </li>
                                            <li><a href="{{url('csr')}}">CSR Policies</a> </li>
                                            <li><a href="{{url('gallery')}}">Gallery</a> </li>
                                        </ul>
                                    </li>
                                    <li  class="dropdown 
                                    {{Request::is('containerised-cargo')||Request::is('loose-cargo')||
                                    Request::is('abnormal-cargo')||Request::is('tracking')?'active':''}}"> <a
                                    class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"
                                    data-animations="fadeInUp">Services <span class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url('containerised-cargo')}}">Containerised Cargo</a> </li>
                                    <li><a href="{{url('loose-cargo')}}">Loose Cargo</a> </li>
                                    <li><a href="{{url('abnormal-cargo')}}">Abnormal Cargo</a> </li>
                                    <li><a href="{{url('tracking')}}">Tracking</a> </li>
                                </ul>
                            </li>

                            <li class="hidden-sm {{Request::is('career')?'active':''}}"><a
                                    href="{{url('career')}}">Careers</a></li>
                            {{-- <li class="hidden-sm {{Request::is('csr')?'active':''}}"><a href="{{url('csr')}}">CSR
                                Policies</a></li> --}}
                            <li class="hidden-sm {{Request::is('qhse')?'active':''}}"><a href="{{url('qhse')}}">QHSE</a>
                            </li>
                            <li class="hidden-sm {{Request::is('contact-us')?'active':''}}"><a
                                    href="{{url('contact-us')}}">Contact Us</a></li>




                        </ul>
                        <div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs"
                            style="position: unset;padding-top: 18px;">
                            <a href="javascript:void(0)" class="btn btn-primary pull-right">request quote</a>
                        </div>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-end -->
            </nav>
        </div>
    </header>