<form action="{{url('request-quote')}}" method="post">
    {{csrf_field()}}
  <div class="row clearfix">

    <!--Form Group-->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <input class="form-control" type="text" placeholder="Contact Name" value=""
            name="contact_name">
    </div>

    <!--Form Group-->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <input class="form-control" type="text" placeholder="Company Name" value=""
            name="company_name">
    </div>

    <!--Form Group-->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <input class="form-control" type="text" placeholder="Email Address" value=""
            name="email">
    </div>

    <!--Form Group-->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <input class="form-control" type="text" placeholder="Phone Number" value=""
            name="phone_number">
    </div>

    <!--Form Group-->
    <div class="form-group col-md-12 col-sm-12 col-xs-12">
        <select class="form-control" name="location_from">
            <option>Select Location - From</option>

            <option>Burundi</option>
            <option>Congo</option>
            <option>Kenya</option>
            <option>Malawi</option>
            <option>Rwanda</option>
            <option>Tanzania</option>
            <option>Uganda</option>
            <option>Zambia</option>
        </select>
    </div>

    <!--Form Group-->
    <div class="form-group col-md-12 col-sm-12 col-xs-12">
        <select class="form-control" name="location_to">
            <option>Select Location - To</option>

            <option>Burundi</option>
            <option>Congo</option>
            <option>Kenya</option>
            <option>Malawi</option>
            <option>Rwanda</option>
            <option>Tanzania</option>
            <option>Uganda</option>
            <option>Zambia</option>
        </select>
    </div>

    <!--Form Group-->
    <div class="form-group col-md-12 col-sm-12 col-xs-12">
        <textarea class="form-control" rows="4" cols="20" placeholder="Cargo Description"
            name="cargo_description"></textarea>
    </div>

    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
        <div class="g-recaptcha" data-sitekey="{{config('recaptcha.site_key')}}"></div>
    </div> 
      <!--Form Group-->
      <div class="form-group col-md-12 col-sm-12 col-xs-12 text-riht"> <button class="custom-button light">Submit Request</button> </div>
  </div>
</form>