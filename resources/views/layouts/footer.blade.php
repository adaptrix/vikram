<footer class="footer-area">

    <!--Footer Upper-->
    <div class="footer-content">
        <div class="container">
            <div class="row clearfix">

                <!--Two 4th column-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-7 col-sm-6 col-xs-12 column">
                            <div class="footer-widget about-widget">

                                <h2>Address</h2>
                                <ul class="contact-info">
                                    <li><span class="icon fa fa-map-marker"></span> Jamhuri Street, Urban Rose Hotel,
                                        <Br> Mazzanine Floor.</li>
                                    <li><span class="icon fa fa-phone"></span> +255 715 439 843</li>
                                    <li><span class="icon fa fa-envelope-o"></span> md@vikramlogistics.co.tz</li>

                                </ul>
                                <div class="social-links-two clearfix">
                                    <a href="#" class="facebook img-circle"><span class="fa fa-facebook-f"></span></a>
                                    <a href="#" class="instagram img-circle"><span class="fa fa-instagram"></span></a>
                                    <a href="#" class="linkedin img-circle"><span class="fa fa-linkedin"></span></a>
                                </div>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="col-lg-5 col-sm-6 col-xs-12 column">
                            <h2>Our Services</h2>
                            <div class="footer-widget links-widget">
                                <ul>
                                    <li><a href="{{url('containerised-cargo')}}">Containerised Cargo</a></li>
                                    <li><a href="{{url('loose-cargo')}}">Loose Cargo</a></li>
                                    <li><a href="{{url('abnormal-cargo')}}">Abnormal Cargo</a></li>
                                    <li><a href="{{url('tracking')}}">Tracking</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Two 4th column End-->

                <!--Two 4th column-->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="col-lg-5 col-sm-6 col-xs-12 column">
                            <div class="footer-widget links-widget">
                                <h2>Site Links</h2>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">CSR Policies</a></li>
                                    <li><a href="#">Gallery</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>

                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="col-lg-6 col-sm-6 col-xs-12 column">
                            <div class="footer-widget links-widget">
                                <img src='images/tat.jpg' />
                            </div>
                        </div>
                    </div>
                </div>
                <!--Two 4th column End-->

            </div>
        </div>
    </div>

    <!--Footer Bottom-->
    <div class="footer-copyright">
        <div class="auto-container clearfix">
            <!--Copyright-->
            <div class="copyright text-center">Copyright 2019 &copy; Designed & Developed by <a target="_blank"
                    href="http://legendaryits.com">Legendary IT Solutions</a></div>
        </div>
    </div>
</footer>




<!-- =-=-=-=-=-=-= Quote Modal =-=-=-=-=-=-= -->
{{-- <div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs">
		  <a class="btn btn-primary" href="javascript:void(0)"><i class="fa fa-envelope"></i></a>
	</div> --}}
<!-- =-=-=-=-=-=-= Quote Modal End =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap Core Css  -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Dropdown Hover  -->
<script src="{{asset('js/bootstrap-dropdownhover.min.js')}}"></script>
<!-- Dropdown Hover  -->
<!-- Jquery Easing -->
<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
<!-- Jquery Counter -->
<script src="{{asset('js/jquery.countTo.js')}}"></script>
<!-- Jquery Waypoints -->
<script src="{{asset('js/jquery.waypoints.js')}}"></script>
<!-- Jquery Appear Plugin -->
<script src="{{asset('js/jquery.appear.min.js')}}"></script>
<!-- Jquery Shuffle Portfolio -->
<script src="{{asset('js/jquery.shuffle.min.js')}}"></script>
<!-- Carousel Slider  -->
<script src="{{asset('js/carousel.min.js')}}"></script>
<!-- Jquery Migrate -->
<script src="{{asset('js/jquery-migrate.min.js')}}"></script>
<!--Style Switcher -->
<script src="{{asset('js/color-switcher.js')}}"></script>
<!-- Gallery Magnify  -->
<script src="{{asset('js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<!-- Sticky Bar  -->
<script src="{{asset('js/theia-sticky-sidebar.js')}}"></script>
<!-- Template Core JS -->
<script src="{{asset('js/custom.js')}}"></script>



<!-- Jquery For Home Page Only  -->
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{asset('js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
{{-- <script src="{{asset('js/custom.js')}}"></script> --}}
<script type="text/javascript">
    $(document).ready(function (e) {
        var revapi = $(".rev_slider").revolution({
            sliderType: "standard",
            jsFileLocation: "js/revolution-slider/js/",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 5000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "gyges",
                    enable: true,
                    hide_onmobile: false,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 0,
                        v_offset: 0
                    }
                },

                bullets: {
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 800,
                    style: "hebe",
                    hide_onleave: false,
                    direction: "horizontal",
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 30,
                    space: 5,
                    tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                }
            },
            responsiveLevels: [1240, 1024, 778],
            visibilityLevels: [1240, 1024, 778],
            gridwidth: [1170, 1024, 778, 480],
            gridheight: [620, 768, 960, 720],
            lazyType: "none",
            parallax: {
                type: "scroll",
                origo: "slidercenter",
                speed: 1000,
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                type: "scroll",
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "on",
            stopAfterLoops: 0,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "0",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    });
</script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
          (Load Extensions only on Local File Systems ! 
           The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript"
    src="{{asset('js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/revolution-slider/js/extensions/revolution.extension.video.min.js')}}">
</script>
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
<!-- =-=-=-=-=-=-= Quote Modal =-=-=-=-=-=-= -->
<div class="modal fade " id="request-quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="quotation-box-1">
                    <h2>REQUEST A QUOTE</h2>
                    <div class="desc-text">
                        <p>Vikram logistics is always ready to assist you accurate and timeless quote to meet all your
                            logistics needs on your tips. Please use the form below to fill all your logistics needs and
                            we will get back you ASAP!</p>
                    </div>
                   @include('layouts.quote-request-form')
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- =-=-=-=-=-=-= Quote Modal End =-=-=-=-=-=-= -->
<script>
    window.onload = function () {
        var $recaptcha = document.querySelector('#g-recaptcha-response');

        if ($recaptcha) {
            $recaptcha.setAttribute("required", "required");
        }
    };
</script>

<script>
    // document.querySelector('.image-placement').addEventListener('mouseenter',function(){
    //     console.log('in');
    // });
    // document.querySelector('.image-placement').addEventListener('mouseleave',function(){
    //     console.log('out');
    // });
    $('.image-placement').on('mouseenter', function () {
        // $(this).find('.description-placement').fadeIn();
        $(this).find('.description-placement').removeClass('fadeOut');
        $(this).find('.description-placement').addClass('fadeInUp');
    });
    $('.image-placement').on('mouseleave', function () {
        $(this).find('.description-placement').removeClass('fadeInUp');
        $(this).find('.description-placement').addClass('fadeOut');
    });
</script>
@if(session('message'))
<script> 
    $(document).ready(function(){
        alertify.notify('{{session('message')}}', 'success', 5, function(){  console.log('dismissed'); });
    });
</script>
@endif
@if(session('error'))
<script> 
    $(document).ready(function(){
        alertify.error('{{session('error')}}');
    });
</script>
@endif