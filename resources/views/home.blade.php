   @extends('layouts.app')

   @section('content')
       
   
   <!-- =-=-=-=-=-=-= HOME SLIDER =-=-=-=-=-=-= -->
    <div class="rev_slider_wrapper">
            <div class="rev_slider" data-version="5.0">
              <ul>
                  <!-- SLIDE 1 -->
                  <li data-index="rs-4" data-transition="crossfade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/slider/thumb-3.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                  <!-- MAIN IMAGE -->
                  <img src="images/slider/1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                  </li>
              
                  <!-- SLIDE 2 -->
                  <li data-index="rs-1" data-transition="crossfade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/slider/thumb-2.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                  <!-- MAIN IMAGE -->
                  <img src="images/slider/2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                  </li>
  
                  <!-- SLIDE 3 -->
                  <li data-index="rs-2" data-transition="crossfade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/slider/thumb-3.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                  <!-- MAIN IMAGE -->
                  <img src="images/slider/3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                  </li>
              </ul>
            </div><!-- end .rev_slider -->
          </div>
      <!--======= HOME SLIDER END =========-->
  
      <!-- =-=-=-=-=-=-= Call To Action =-=-=-=-=-=-= -->
      <div class="parallex-small " style="background: #2275B1 none repeat scroll 0 0 !important;">
          <div class="container">
              <div class="row custom-padding-20 ">
                  <div class="col-md-8 col-sm-8">
                      <div class="parallex-text">
                          <h4>Not sure which solution fits you business needs?</h4>
                      </div>
                      <!-- end subsection-text -->
                  </div>
                  <!-- end col-md-8 -->
  
                  <div class="col-md-4 col-sm-4">
                      <div class="parallex-button"> 
                              <div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs" style="position: unset;">
                                      <a href="#quote" class="page-scroll btn btn-lg btn-clean">Get a quote <i class="fa fa-angle-double-right "></i></a> 
                                  </div>
                          
                      </div>
                      <!-- end parallex-button -->
                  </div>
                  <!-- end col-md-4 -->
  
              </div>
              <!-- end row -->
          </div>
          <!-- end container -->
      </div>
      <!-- =-=-=-=-=-=-= Call To Action End =-=-=-=-=-=-= -->
  
      <section class="section-padding-70" id="about">
          <div class="container">
              <div class="row clearfix">
  
                  <!--Column-->
                  <div class="col-md-7 col-sm-12 col-xs-12 ">
                      <div class="about-title">
                          {{-- <h3>who we are</h3> --}}
                          <h2>About Our Company</h2>
                          <p>Vikram Logistics Tanzania Limited ( herein referred as “VLT”) is young and dynamic company with experienced management team which aims to become a leading integrated global logistics and supply chain management company providing business enabling solutions and exceeding customer expectations. The company value on innovative logistic approach with professional work ethics.</p>
                          <p>VLT Services include port and custom clearance services, container storage services and transportation within Tanzania and neighboring countries including Kenya, Uganda, Rwanda, Burundi, Democratic Republic of Congo, Zambia, and Malawi. We believe to be as "one stop logistics service provider" for our customers.</p>
                          <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('about')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                      </div>
  
                  </div>
  
                  <!-- Quote Form -->
                  <div class="col-md-5 col-sm-12 col-xs-12 our-gallery" style="margin-top: 40px;">
                      <img class="img-responsive " alt="Image" src="images/about-us.jpg">
                      <p style="text-align: center;font-size: 23px;font-style: italic;font-family: cursive;">Keeping Your Load On Road</p>
                  </div>
              </div>
          </div>
      </section>    

        <section class="parallex our-app section-padding-100">
            <div class="container">
                <div class="row"> 
                    <!-- Section Content -->
                    <div class="section-content">
                        
                        <div class="col-md-12 col-sm-12 text-left" style="text-align: justify;">
                        {{-- <h3>Download Our Tracking App</span></h3> --}}
                        <p>From either short haul or a long haul, Vikram logistics are pleased to serve all your logistics needs through out the central and East Africa, our fleet consist of brand new, reliable and well maintained heavy duty vehicles that are best suitable for all our African routes. Our reliable fleet allows the team a stress free and accurate round a clock service deliver times for all  the transit and local cargo clients. Our fleet adheres international safety standards and include the following vehicle and trailer types</p>
                        <ul class="list-inline">
                            <li><a href="#" class="btn btn-bordered">6x2 Heavy Tractors Trucks</a></li>
                            <li><a href="#" class="btn btn-bordered">6x4 Heavy Tractors Trucks</a></li>
                            <li><a href="#" class="btn btn-bordered">40” Heavy duty flatbed trailer</a></li>
                            <li><a href="#" class="btn btn-bordered">Escort and Rapid Response Vehicle</a></li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  
      <!-- =-=-=-=-=-=-= Our Services =-=-=-=-=-=-= -->
      <section class="custom-padding services">
          <div class="container">
              <!-- title-section -->
              <div class="main-heading text-center">
                  <h2>our services</h2>
                  <p>We always keep you up-to-date. We guarantee permanent information flow for all persons involved in the IT-process by connecting with the customer-specific electronic data processing systems. We have our own fleet of trucks and wide fleet park of our subcontracted partners at use for execution of our customers’ demands.</p>
              </div>
              <!-- End title-section -->
              
              <!-- Row -->
              <div class="main-boxes">
                  <div class="row owl-carousel-3">
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a href="{{url('containerised-cargo')}}"><img alt="" src="images/1.png"></a>
                              </div>
                              <div class="services-text">
                                  <h4>CONTAINERISED CARGO</h4>
                                  <p>Our company’s containerized cargo division specializes in the movement of a variety of intermodal containers, which allows us to transport large volumes of cargo anywhere around East and Central Africa </p>
                              </div>
                              <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('containerised-cargo')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->
  
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a href="{{url('loose-cargo')}}"><img alt="" src="images/2.png"></a>
                              </div>
                              <div class="services-text">
                                  <h4>LOOSE CARGO</h4>
                                  <p>With an extensive history in transportation of loose bulk cargo, VLT has always led by the example in the delivery of unpacked deliveries and mass commodity transport in the East African Sahara. </p>
                              </div><br>
                              <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('loose-cargo')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->
  
                      <div class="col-sm-12 col-md-12 col-xs-12">
                          <div class="services-grid-1">
                              <div class="service-image">
                                  <a href="{{url('abnormal-cargo')}}"><img alt="" src="images/3.png"></a>
                              </div>
                              <div class="services-text">
                                  <h4>ABNORMAL CARGO</h4>
                                  <p>Confident in the movement of cargo of all shapes and sizes, VLT has the capacity, expertise and equipment to accommodate your specific transporting needs. </p>
                              </div><br>
                              <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('abnormal-cargo')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                          </div>
                          <!-- end services-grid-1 -->
                      </div>
                      <!-- end col-sm-4 -->

                      <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="services-grid-1">
                            <div class="service-image">
                                <a href="{{url('tracking')}}"><img alt="" src="images/3.png"></a>
                            </div>
                            <div class="services-text">
                                <h4>TRACKING</h4>
                                <p>Here at VLT, We offer our esteemed clients exclusive live tracking and tracing access with guaranteed round the clock on the conditions and location of all all your consignments.</p>
                                
                            </div><br>
                            <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('tracking')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                        <!-- end services-grid-1 -->
                    </div>
                    <!-- end col-sm-4 -->
  
                  </div>
              </div>
              <!-- Row End -->
          </div>
          <!-- end container -->
      </section>
      <!-- =-=-=-=-=-=-= Our Services-end =-=-=-=-=-=-= -->
  
      <!-- =-=-=-=-=-=-= Quote Seection =-=-=-=-=-=-= -->
  
      <section class="quote" id="quote">
          <div class="container">
              <div class="row clearfix">
  
                  <!--Column-->
                  <div class="col-md-7 col-sm-12 col-xs-12 ">
                      <div class="choose-title">
                          {{-- <h3>More about us</h3> --}}
                          <h2>Why Vikram Logistics</h2>
                          <p>At VLT, we believe the customer comes first; we are focused to ensure customer delight, superior quality of delivery and increased customer profitability.</p>
                      </div>
                      <div class="choose-services">
                          <ul class="choose-list">
  
                              <!-- feature -->
                              <li>
                                  <div class="choose-box"> <span class="iconbox"><i class="flaticon-logistics-delivery-truck-and-clock"></i></span>
                                      <div class="choose-box-content">
                                          <h4>Fast Worldwide delivery</h4>
                                          <p>At VLT, at our operations all over the country, we practice an enduring value system based on an open culture, honest and fair business and personal conduct, earning the confidence and trust of our Associates and Customers.</p>
                                      </div>
                                  </div>
                              </li>
  
                              <!-- feature -->
                              <li>
                                  <div class="choose-box"> <span class="iconbox"><i class="flaticon-call-center-worker-with-headset"></i></span>
                                      <div class="choose-box-content">
                                          <h4>Safety & Compliance</h4>
                                          <p>At VLT, we practice transparency with all agencies that we are involved with. We value the importance of our colleagues, evolving a sense of togetherness and passion to deliver.
                                                  </p>
                                      </div>
                                  </div>
                              </li>

                              <li>
                                    <div class="choose-box"> <span class="iconbox"><i class="flaticon-person-standing-beside-a-delivery-box"></i></span>
                                        <div class="choose-box-content">
                                            <h4>Quality Of Delivery</h4>
                                            <p>At VLT, we believe the customer comes first; we are focused to ensure customer delight, superior quality of delivery and increased customer profitability.
                                                    </p>
                                        </div>
                                    </div>
                                </li>
                          </ul>
                          <!-- end choose-list -->
                      </div>
                  </div>
  
                  <!-- Quote Form -->
                  <div class="col-md-5 col-sm-12 no-extra col-xs-12">
                      <div class="quotation-box">
                          <h2>REQUEST A QUOTE</h2>
                          <div class="desc-text">
                              <p>Vikram logistics is always ready to assist you accurate and timeless quote to meet all your logistics needs on your tips. Please use the form below to fill all your logistics needs and we will get back you ASAP!</p>
                          </div>
                          @include('layouts.quote-request-form')
                      </div>
                  </div>
              </div>
          </div>
      </section>
  
      <style>
          .image-placement{
              background-color: white;
              padding: 80px 20px;
              cursor:pointer;
          }
          .image-placement img{ 
              height: 180px;
          }
          #text-placement{
              background-color: #010101; 
              padding: 10px;
              cursor: pointer;
          }
          .description-placement{
              position: fixed;
              bottom: 51px;
              right: 0px;
              /* display: none;   */
              opacity: 0;
              background-color: #010101c7;
              padding: 20px;           
              cursor: pointer;
          }
          .description-placement:hover{
              display: block;
          }

          .item{
              padding: 5px;
          }
      </style>
      <!-- =-=-=-=-=-=-= Quote Seection end=-=-=-=-=-=-= -->
      
      <!-- =-=-=-=-=-=-= PARALLEX TESTIMONILS =-=-=-=-=-=-= -->
    <section  class="testimonial-bg  section-padding text-center">
            <div class="container">
                <div id="testimonials" class="owl-carousel">
                    <div class="item">
                        <div class="col-sm-12 col-md-12 col-xs-12 testimonial-grid text-center" style="padding:0px;"> 
                            <div class="image-placement">
                                <img src="images/clients/client_1.png" class="img-responsive" alt="Testimonials">
                                <div class="description-placement animated"> 
                                    <div class="row">
                                        <div class="col-lg-12 text-left">
                                            <i class="fa fa-quote-left"></i>
                                        </div>
                                    </div>
                                    <p style="text-align:justify">Vikram logistics Tanzania Limited provides long haul transport
                                        solution for Tanzania road haulage (1980) by collecting cargo
                                        from our ICD in Tanzania and deliver it to our sites in the eastern
                                        and central Africa, working with them has always been so easy,
                                        once the cargo has been loaded, delivery is made like clockwise.
                                        We receive tracking reports everyday and progress is clearly
                                        specified. Without question or equivocation, Vikram logistics
                                        have always been the choice for all our long haulage needs.   </p> 
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <i class="fa fa-quote-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="text-placement"> 
                                <div class="name">TANZANIA ROAD HAULAGE</div>
                            </div>                     
                            {{-- <div class="profession">Founder, Oxford</div> --}}
                        </div>
                    </div>  
                    <div class="item">
                        <div class="col-sm-12 col-md-12 col-xs-12 testimonial-grid text-center" style="padding:0px;"> 
                            <div class="image-placement">
                                <img src="images/clients/client_4.png" class="img-responsive" alt="Testimonials">
                                <div class="description-placement animated"> 
                                    <div class="row">
                                        <div class="col-lg-12 text-left">
                                            <i class="fa fa-quote-left"></i>
                                        </div>
                                    </div>
                                    <p>Over a long period now, we had the opportunity to work with
                                        Mr. Vishal Mehta, Director of Vikram Logistics to haul many of
                                        our loads. Without any question their service has been excellent.
                                        I would highly recommend to anyone, but especially those who
                                        have been in the industry. “Keep it up” the Team of Vikram Logistics
                                        Tanzania Limited.  </p> 
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <i class="fa fa-quote-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="text-placement">
                                <div class="name">PAMYCO</div>
                            </div>                     
                            {{-- <div class="profession">Founder, Oxford</div> --}}
                        </div>
                    </div>  
                    <div class="item">
                        <div class="col-sm-12 col-md-12 col-xs-12 testimonial-grid text-center" style="padding:0px;"> 
                            <div class="image-placement">
                                <img src="images/clients/client_2.png" class="img-responsive" alt="Testimonials">
                                <div class="description-placement animated"> 
                                    <div class="row">
                                        <div class="col-lg-12 text-left">
                                            <i class="fa fa-quote-left"></i>
                                        </div>
                                    </div>
                                    <p>Vikram logistics has proven to be a very dependable logistics
                                        company. It has been working with AK Transport Company Limited
                                        by collecting bulk of cargo from several places within and Outside
                                        of Tanzania. This is highlighted by their excellent communication,
                                        daily cargo status reporting and sense of urgency on every haul.
                                        Their dispatchers and customer service have proven to be very
                                        trustworthy and professionals.  </p> 
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <i class="fa fa-quote-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="text-placement"> 
                                <div class="name">AK TRANSPORT</div>
                            </div>                     
                            {{-- <div class="profession">Founder, Oxford</div> --}}
                        </div>
                    </div>  
                    <div class="item">
                        <div class="col-sm-12 col-md-12 col-xs-12 testimonial-grid text-center" style="padding:0px;"> 
                            <div class="image-placement">
                                <img src="images/clients/client_3.png" class="img-responsive" alt="Testimonials">
                                <div class="description-placement animated"> 
                                    <div class="row">
                                        <div class="col-lg-12 text-left">
                                            <i class="fa fa-quote-left"></i>
                                        </div>
                                    </div>
                                    <p>We find that Vikram Logistics Tanzania Limited are very efficient
                                        and provide us a very good quality service. They are very prompt
                                        and organized which keeps us very happy  </p> 
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                            <i class="fa fa-quote-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="text-placement"> 
                                <div class="name">JEYJEY</div>
                            </div>                     
                            {{-- <div class="profession">Founder, Oxford</div> --}}
                        </div>
                    </div>  
                      
                     
                </div>
            </div>
            <!-- end container -->
        </section>
        <!-- =-=-=-=-=-=-= PARALLEX TESTIMONILS END =-=-=-=-=-=-= -->
      
  
      <!-- =-=-=-=-=-=-= Blog & News =-=-=-=-=-=-= -->
      <section id="blog" class="custom-padding">
          <div class="container">
              <!-- title-section -->
              <div class="main-heading text-center">
                  <h2>NEWSROOM</h2>
                  {{-- <p>Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi <br> eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit</p> --}}
              </div>
              <!-- End title-section -->
  
              <!-- Row -->
              <div class="row">
                  <div class="col-sm-12 col-xs-12 col-md-12">
  
                      <!-- blog-grid -->
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          <div class="news-box">
                              <div class="news-thumb">
                                <a href="{{url('news1')}}"><img alt="" class="img-responsive" src="images/blog/1.jpg"></a>
                                {{-- <div class="date"> <strong>21</strong>
                                <span>Jun</span> </div> --}}
                              </div>
                              <div class="news-detail">
                                  <h2><a title="" href="{{url('news1')}}">1450 tonnes of load from DRC TO DAR</a></h2>
                                  <p>This month our contract with a mining industry got finalized, and they commenced its copper production in Kalukuluku and Chemaf. </p>
                                  <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news1')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                              </div>
                              
                          </div>
                      </div>
  
                      <!-- blog-grid end -->
  
                      <!-- blog-grid -->
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          <div class="news-box">
                              <div class="news-thumb">
                                  <a href="{{url('news2')}}"><img alt="" class="img-responsive" src="images/blog/2.jpg"></a>
                                  {{-- <div class="date"> <strong>21</strong>
                                    <span>Jun</span> </div> --}}
                              </div>
                              <div class="news-detail">
                                  <h2><a title="" href="{{url('news2')}}">Zambia branch coming soon!!</a></h2>
                                  <p>As a matter of continuous growth and expansion, we are very pleased to announce that the further expansion of new fleet has already been planned and we shall soon grow our presence in Zambia. </p>
                                  <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news2')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                              </div>
                          </div>
                      </div>
                      <!-- blog-grid end -->
  
                      <!-- blog-grid -->
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          <div class="news-box">
                              <div class="news-thumb">
                                  <a href="{{url('news3')}}"><img class="img-responsive" alt="" src="images/blog/3.jpg"></a>
                                  {{-- <div class="date"> <strong>21</strong>
                                    <span>Jun</span> </div> --}}
                              </div>
                              <div class="news-detail">
                                  <h2><a title="" href="{{url('news3')}}">Giving back to the Community</a></h2>
                                  <p>As a part of CSR Programs, The Management of VLT is on mission to host “ Health and Education” program for their employees and communities at large.</p>
                                  <div class="more-about"> <a class="btn btn-primary btn-lg" href="{{url('news3')}}">Read More <i class="fa fa-chevron-circle-right"></i></a> </div>
                              </div>
                          </div>
                      </div>
                      <!-- blog-grid end -->
  
                      <div class="clearfix"></div>
                  </div>
              </div>
  
              <!-- Row End -->
          </div>
          <!-- end container -->
      </section>
      <!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->
  
      <!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
      <section class="section-padding-70 services" style="padding: 20px 0;">
          <div class="container">
              <!-- title-section -->
              <div class="main-heading text-center">
                  <h2 style="margin-bottom: -55px;">our Clients</h2>
                  {{-- <p>Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi <br> eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit</p> --}}
              </div>
              <!-- End title-section -->
  
              <!-- Row -->
              <div class="row">
                    <div id="clients" class="text-center">
                        <!-- Service Item List -->
                        <div class="item">
                            <div class="clients-grid"> <img class="img-responsive" alt="" src="images/clients/client_1.png"> </div>
                        </div>
                        <div class="item">
                            <div class="clients-grid"> <img class="img-responsive" alt="" src="images/clients/client_2.png"> </div>
                        </div>
                        <div class="item">
                            <div class="clients-grid"> <img class="img-responsive" alt="" src="images/clients/client_3.png"> </div>
                        </div>
                        
                        <div class="item">
                            <div class="clients-grid"> <img class="img-responsive" alt="" src="images/clients/client_4.png"> </div>
                        </div>
                    </div>
                      <!-- Service Item List End -->
                  </div>
              </div>
              <!-- Row End -->
          </div>
          <!-- end container -->
      </section>

      @endsection
