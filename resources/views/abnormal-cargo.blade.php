@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>Our expertise </h3>
                    <h2>Abnormal Cargo</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                    <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">Abnormal Cargo</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Our Services =-=-=-=-=-=-= -->
<section class="section-padding-70 services-2">
    <div class="container">
        <!-- Row -->
        <div class="row">

            <div class="col-md-9 col-sm-12 col-md-push-3 col-xs-12">

                <div id="post-slider" class="owl-carousel owl-theme margin-bottom-30">
                    <div class="item">
                        <a class="tt-lightbox" href="images/services/service-detail-2.jpg"><img class="img-responsive" src="images/services/service-detail-2.jpg" alt=""></a>
                    </div>
                    {{-- <div class="item">
                        <a class="tt-lightbox" href="images/services/service-detail-1.jpg"><img class="img-responsive" src="images/services/service-detail-1.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a class="tt-lightbox" href="images/services/service-detail-3.jpg"><img class="img-responsive" src="images/services/service-detail-3.jpg" alt=""></a>
                    </div>
                    <div class="item">
                        <a class="tt-lightbox" href="images/services/service-detail-4.jpg"><img class="img-responsive" src="images/services/service-detail-4.jpg" alt=""></a>
                    </div> --}}
                </div>

                <p>Confident in the movement of cargo of all shapes and sizes, VLT has the capacity, expertise and equipment to accommodate your specific transporting needs. With vast experience in handling, loading and delivering large quantities of irregular sized cargo, VLT has developed partnerships with the agricultural, mining and construction industries. Our company operates a diverse range of multi axle low loader trailers as well as extendable units, which allow for optimum payload with minimal effort. Our out of gage cargo package also offers a supervised escort service ensuring a protected delivery.</p>
            </div>
            <!-- right column -->
            <div class="col-md-3 col-md-pull-9 col-sm-12 col-xs-12" id="side-bar">
                <div class="theiaStickySidebar">
                    <div class="side-bar-services">
                        <ul class="side-bar-list">
                            <li><a href="{{url('containerised-cargo')}}" >Containerised Cargo</a></li>
                            <li><a href="{{url('loose-cargo')}}" >Loose Cargo</a></li>
                            <li><a href="{{url('abnormal-cargo')}}" class="active">Abnormal Cargo</a></li>
                            <li><a href="{{url('tracking')}}">Tracking</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- end container -->
</section>
<!-- =-=-=-=-=-=-= Our Services-end =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Call To Action =-=-=-=-=-=-= -->
<div class="parallex-small ">
    <div class="container">
        <div class="row custom-padding-20 ">
            <div class="col-md-8 col-sm-8">
                <div class="parallex-text">
                    <h4>Not sure which solution fits you business needs?</h4>
                </div>
                <!-- end subsection-text -->
            </div>
            <!-- end col-md-8 -->

            <div class="col-md-4 col-sm-4">
                <div class="parallex-button"> 
                        <div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs" style="position: unset;">
                                <a href="#quote" class="page-scroll btn btn-lg btn-clean">Get a quote <i class="fa fa-angle-double-right "></i></a> 
                            </div>
                    
                </div>
                <!-- end parallex-button -->
            </div>
            <!-- end col-md-4 -->

        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- =-=-=-=-=-=-= Call To Action End =-=-=-=-=-=-= -->

@endsection