@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>Our news </h3>
                    <h2>1450 Tonnes Of Load From DRC TO DAR</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">1450 Tonnes Of Load From DRC TO DAR</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Blog & News =-=-=-=-=-=-= -->
<section id="blog" class="custom-padding">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Left Content Area -->
            <div class="col-sm-12 col-xs-12 col-md-8">


                <!-- blog-grid -->
                <div class="news-box no-space">
                    <!-- post image -->
                    <div class="news-thumb">
                        <!-- slider post -->
                        <div id="post-slider" class="owl-carousel owl-theme">
                            <div class="item"><img class="img-responsive" src="images/blog/detail-1.jpg" alt=""></div>
                        </div>
                    </div>
                    <!-- post image end -->

                    <!-- blog detail -->
                    <div class="news-detail single">
                        <h2><a title="" href="#">1450 TONNES OF LOAD FROM DRC TO DAR</a></h2>

                        <p>
                            This month our contract with a mining industry got finalized, and they commenced its copper production in Kalukuluku and Chemaf ready to transport cargo to Dar es salaam. Where our team worked together with the national road authority and acquired all the necessary permits and licenses to load 1750 tones of loose copper from Kalukuluku to Dar es salaam and ensured a proper supply chain of distribution without any failure.
                        </p>
                    </div>
                    <!-- blog detail end -->

                </div>
            </div>
            <!-- Left Content Area -->

            <!-- Right Sidebar Area -->
            <div class="col-sm-12 col-xs-12 col-md-4">

                <!-- sidebar -->
                <div class="side-bar" style="padding-top: 0px;">
                    <!-- widget -->


                    <!-- widget -->
                    <div class="widget">
                        <div class="latest-news">
                            <h2>Latest News</h2>

                            <div class="post">
                                <figure class="post-thumb"><img alt="" src="images/blog/small-1.png"></figure>
                                <h4><a href="{{url('news2')}}">Zambia Branch Coming Soon!!</a></h4>
                            </div>
                            <br>
                            <div class="post">
                                <figure class="post-thumb"><img alt="" src="images/blog/small-2.png"></figure>
                                <h4><a href="{{url('news3')}}">Giving Back To The Community</a></h4>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- sidebar end -->

            </div>
            <!-- Right Sidebar Area End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- end container -->
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->

@endsection