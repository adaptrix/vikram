@extends('layouts.app')

@section('content')

    <!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
    <section class="breadcrumbs-area parallex">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <div class="col-sm-12 col-md-6 page-heading text-left">
                        <h3>Our Creatives</h3>
                        <h2>Our Team Members</h2>
                    </div>
                    <div class="col-sm-12 col-md-6 text-right">
                        <ul class="breadcrumbs">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="#">Our Team</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->
    <section id="team" class="custom-padding">
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/1.jpg">
                        </div>
                        <div class="team-content">
                            {{-- <h2>Kalpesh Mehta</h2> --}}
                            <img src="images/Signature.png" style="width:90%;height:90px; object-fit:contain;" alt="">
                            <p style="text-align:center; margin-left:-10px;">CEO</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="about-title">
                        <h2>Message From CEO</h2>
                        <p>In current scenario of geo-political ambiguity, business faces the most challenging times, unforeseen hurdles across the globe. Tanzania stands out as an exception due to its exceptional market conditions, geographical connectivity, and growth oriented economic conditions. We, at Vikram Logistics, believe in taking up each challenge as an opportunity and strive to find solutions with our innovative ideas, ingenuity and passion. This helps us to translate our hard work to the desired solutions. With this approach, we have been enormously successful in surpassing our clients` expectations. We are ardent about our error- free, cost effective, efficient and timely logistics services.</p>
                        <p>We always look for hiring the best employees to deliver the active solutions to our client. Furthermore, we are people centric and we recognize that we will grow as an organization by empowering our employees by giving them well deserved rewards and space to develop as an effective leader. This helps us gain the trust of our clients, as our services are integrated into their businesses and we function as an external partner for their core processes.</p>
                        <p>One of our goals is to promote sustainable living. We are developing new ways of doing business with the aim of reducing our environ¬mental footmark and increasing our positive social impact. When it comes to fulfilling our responsibilities towards communities and the society at large, Vikram Logistics is looking for arranging various social events to uplift the retrograde people by providing them equal opportunities to enjoy their life and by this means contributing its share towards Corporate Social Responsibility. </p>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p>We embrace new information technology developments to stay competitive, flexible and to serve best to our client’s changing requirements and aspirations. We uphold high standards of services with the help of latest technological know-how and implement global fair-trade practices. </p>
                    <p>To conclude, our eyes are clearly set on the future and we are staunch believers in long term relationships be it with our clients, employees or business partners. We want to increase competitiveness to achieve industry leading growths and margins. </p>
                    <p>I would also like to express my gratitude to our business partners, vendors, employees and the government, for their unceasing support throughout the years.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="padding-top-70" id="about">
        <div class="container">
            <div class="row clearfix">
                <!--Column-->
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        <h2>Meet The Team</h2>
                        <p>As with any great company, the foundation of our past, present, and future success, is a direct reflection of the hard work, dedication, creativity, and loyalty of our people from all across the country. We look for quality professionals who take great pride in everything they do here at VLT.  At the end of the day, we want our people to know they are a part of something very special, and a vital part of a company that genuinely cares about them.</p>
                        <p>Managed by a Experienced Directors, group of professionals in Logistics and Shipping company, VLT strive to maintain quality throughout our organization and with our trained staff; we will meet or exceed the expected standards of performance placed on us by our clients.</p>
                        <p>Our People take ownership of their responsibilities and are dedicated to being an integral part of the always growing team at VLT This sense of pride is evident in all phases of our business, and is a direct reflection on why we have seen such tremendous growth and success in recent years.  We value the opinions and input from our workforce, and we use this feedback to create a more dynamic, resilient workplace.</p>
                        <p>We manage our logistic firm operations through our professional and dedicated team leaders and therefore this allows a single point of contact for you and your partners to communicate. We offer a very flexible operational setup with standardized processes which allows your business to benefit from the best practices across different sectors. </p>
                        <p>The culture of operational excellence is embedded in everything we do across our operations. </p>
                        <p>Our global network staffed by knowledgeable specialists will ensure your cargo is delivered on time, and with the utmost care.</p>
                        <p>VLT has the enviable reputation for having very low driver turnover.  In the trucking industry this can only be achieved by having a culture where respect, trust, honesty and fairness are at the center of how we work.  These leadership qualities create pride in how our drivers do their jobs, and it is this pride that creates our culture.</p>
                        <p>VLT is always working and moving forward in the best interests of our customers. Our people’s commitment, drive and determination to get the job done is what keeps your load on the road.</p>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="team" class="custom-padding">
        <div class="container">

            <!-- Row -->
            <div class="row">
                <!-- Team Grid -->

                
                {{-- <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/1.jpg">
                        </div>
                        <div class="team-content">
                            <h2>Kalpesh Mehta</h2>
                            <p>CEO</p>
                        </div>
                    </div>
                </div> --}}


                <!-- Team Grid End-->
                <!-- Team Grid -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/2.jpg">
                        </div>
                        <div class="team-content">
                            <h2>Vishal Mehta</h2>
                            <p><i>MANAGING DIRECTOR</i></p>
                        </div>
                    </div>
                </div>
                <!-- Team Grid End-->
                <!-- Team Grid -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/3.jpg">
                        </div>
                        <div class="team-content">
                            <h2>Mohammed Kara</h2>
                            <p><i>FINANCE MANAGER</i></p>
                        </div>
                    </div>
                </div>
                <!-- Team Grid End-->
                

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/4.jpg">
                        </div>
                        <div class="team-content">
                            <h2>Mavalla Msuya</h2>
                            <p><i>COMPLIANCE MANAGER</i></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/5.png">
                        </div>
                        <div class="team-content">
                            <h2>Ezekiel Mestiack</h2>
                            <p><i>HEAD OF OPERATIONS</i></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/6.jpg">
                        </div>
                        <div class="team-content">
                            <h2>Veronica George</h2>
                            <p><i>HEAD OF HUMAN RESOURCES</i></p>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="team-grid">
                        <div class="team-image">
                            <img alt="" class="img-responsive" src="images/team/7.png">
                        </div>
                        <div class="team-content">
                            <h2>Emmanuel Boaz</h2>
                            <p><i>PROCUREMENT MANAGER</i></p>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Our Team End =-=-=-=-=-=-= -->

@endsection