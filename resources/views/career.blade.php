@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>Career With Vikram Logistics</h3>
                    <h2>Career</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="#">Career</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= About Section =-=-=-=-=-=-= -->

<section class="padding-top-70" id="about" style="padding-bottom: 30px;">
    <div class="container">
        <div class="row clearfix">
            <!--Column-->
            <div class="col-md-8 col-sm-12 col-xs-12 ">
                <div class="about-title">
                    {{-- <h2 style="font-size: 20px;">Quality, health, safety, environment</h2> --}}
                    <p>
                        At Vikram Logistics, we are very serious about what we do, and always looking how things can be
                        done better. Beginning your carrier at Vikram Logististics means being part of an innovative, a
                        customer focused and well driven company. our company offers dynamic environment, possibility of
                        professional growth and education across all the segments of logistics operation. Our team
                        brings together a wealth of experience from a business and professional perspective with an
                        intention of being a logistics firm that works round a clock with an objective of keeping
                        operations, customers experience and services better.
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 ">
                <div class="about-title">

                    <h2 style="font-size: 20px;color:#777">The Value We Live By</h2>


                    <ol class="rectangle-list">
                        <li><a href="">Openness & Honesty</a></li>
                        <li><a href="">Trust & Respect</a></li>
                        <li><a href="">Customer Focus</a></li>
                        <li><a href="">Integrity</a></li>
                        <li><a href="">Compassion</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="about-title">
                    {{-- <h2 style="font-size: 20px;">Quality, health, safety, environment</h2> --}}

                    <p>
                        As a commitment of our ongoing commitment and growth, Vikram logistics also supports on going
                        training and by providing verities of in house skill training which is important for their
                        personal development as well as for the advancement of the firm
                    </p>
                    <p>
                        Our team is the most important asset of our company and the core of all the achievements of the
                        past and future depend largely on their commitment and contribution. We believe "If everyone is
                        moving forward together, then success takes care of itself." We take time to understand our
                        employees and draw on their experience to proudly maintain a culture of collaboration by valuing
                        every individual’s viewpoint and work hard to surpass the best in the world. We take
                        responsibility for our actions and always led by the example, acting in the best interest of our
                        employees and customers, when things go wrong we hold up our hands and work hard to put things
                        right. Career with Vikram as a part of your professional development, we provide plenty of
                        unique challenges and opportunities to develop your role and gain knowledge and experience.
                        We'll also ensure we reward your hard work at a competitive rate.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =-=-=-=-=-=-= About End =-=-=-=-=-=-= -->



<section class="section-padding-70 gray" id="why-choose">
    <div class="container">
        <div class="row clearfix">
            <!-- Quote Form -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="accordion-box style-one">
                    <div class="faqs-title">
                        {{-- <h3>need help</h3> --}}
                        <h2>Current Job Openings</h2>
                        <p></p>
                    </div>
                    @php
                    $job_application_id = "logistics";
                    $job_title = "Logistics Operation Manager";
                    @endphp
                    @include('layouts.job-application-form')
                    <!-- Accordion -->
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Logistics Operation Manager</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Education, Skills & Qualifications</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Take the lead role in planning, costing and allocating routes by Ensuring vehicles
                                    sufficient flexibility is in place.</li>
                                <li>Should be responsible for managing the execution, direction, and coordination of all
                                    transportation matters within the organization. (This includes managing budgets,
                                    organizing schedules & routes, ensuring that vehicles are safe and meet legal
                                    requirements and making sure that drivers are aware of their duties.</li>
                                <li>Tracking of Trucks on Road.</li>
                                <li>Trucking of the fleet and insure the trucks have covered 500 Kms a day.</li>
                                <li>Should able to make Weekly Loading Plans for the trucks and the Cargo.</li>
                                <li>Preparing of Road Consignment Notes and record keeping.</li>
                                <li>Handling Drivers and their issues on road.</li>
                                <li>Should have fully knowledge of Project cargo on Loading Plans and applying of
                                    permits.</li>
                                <li>Should be flexible in Logistics industry (this including port visiting, site
                                    visiting, cliental Meetings)</li>
                                <li>Should have Minimum Experience of 5 years in Logistics industry.</li>
                                <li>Should have knowledge of all Transit rules and Regulations</li>
                                <li>Client coordination.</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#{{$job_application_id}}" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "workshop";
                    $job_title = "Workshop Managers position and duty assigned ";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Workshop Managers position and duty assigned </h4>
                        </div>

                        <div class="accord-content">
                            <h3><b>Education, Skills & Qualifications</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Should be responsible for the general servicing, maintenance, repair of the vehicles
                                    and investigating causes of breakdowns if any.</li>
                                <li>Should have 5 + years of experience in Transport and Logistics service support and
                                    experience in all type of vehicle brands.</li>
                                <li>Should be Proficient in Microsoft Office software including Excel, Word, and
                                    PowerPoint</li>
                                <li>Should Maintain schedules of the workshop hours on day to day bases.</li>
                                <li>Should organize and supervise the servicing, maintenance and repair of the company’s
                                    fleet and ensure that the vehicles are in good working conditions at all times.</li>
                                <li>Shall ensure that the vehicles come to the workshop at the designated kilometres for
                                    servicing.</li>
                                <li>Shall investigate and diagnose the causes of vehicle breakdown and accidents and
                                    apportion blames accordingly by ensuring that defaulters take financial
                                    responsibility of the cost of repairs.</li>
                                <li>Should ensure that the workshop is functional at all times and that vehicles
                                    awaiting repairs are attended to with dispatch.</li>
                                <li>Should provide purchasing manager periodically with the list of parts that are
                                    frequently used for replacement in order to avoid stock outs.</li>
                                <li>Vehicles for maintenance must be assigned to a designated Mechanic(s) and proper
                                    note taken in case the vehicles fails.</li>
                                <li>Should Design a working time sheet to ensure vehicles do not exceed the required
                                    time once inside workshop. At least 24hours time the truck should be out of
                                    workshop.</li>
                                <li>Should ensure the mobile maintenance team/Vehicle is always available when called
                                    upon. </li>
                                <li>Recover to base all broken down vehicles or accidental trucks/trailers with
                                    dispatch.</li>
                                <li>Ensure that the truck/trailer are thoroughly checked and bolt and nuts retightened
                                    on the arrival of the vehicles.</li>
                                <li>Should organize training for the workshop team and helpers to improve the quality of
                                    service.</li>
                                <li>Shall coordinate the activities of Mechanics and instill discipline on them.</li>
                                <li>Ensure that all the tools and equipment in the workshop are functional and are being
                                    prudently used.</li>
                                <li>Shall organize training in conjunction with the training unit for all the drivers
                                    especially on how to handle minor break downs.</li>
                                <li>Must avoid pilfering of the company’s tools and materials.</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#{{$job_application_id}}" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "drivers";
                    $job_title = "Drivers";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Drivers </h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Education, Skills & Qualifications</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Class CE driving license with PSV</li>
                                <li>3 years’ relevant work experience with a Sino truck (HOWO)</li>
                                <li>Valid international passport and yellow fever card</li>
                                <li>Reference letter from former employer</li>
                                <li>Recommendation letter from 2 civil servants</li>
                                <li>2 passport size photos</li>
                                <li>Application letter</li>
                                <li>CV</li>
                                <li>Aged between 25-50</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#{{$job_application_id}}" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "assistant";
                    $job_title = "Assistant Driver Trainer ";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Assistant Driver Trainer</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Key Role:</b></h3>
                            <p>Assisting driver with loading and offloading the cargo, delivering equipment or products
                                to customers and clients, helping drivers navigate to each destination on schedule, and
                                recording delivery logs.</p>
                            <h3><b>Reporting Problems</b></h3>
                            <p>any issues with a delivery or problems with a deadline, it’s a driver helper’s
                                responsibility to report it to a supervisor or senior staff member and help resolve the
                                problem if possible. </p>
                            <p>To ensure training document control and records are updated and current.</p>
                            <p>Coordinate and plan for daily instruction, insuring proper utilization of equipment and
                                drivers.</p>
                            <p>Conduct probation reviews and provide clear recommendations for further training or
                                dismissal.</p>
                            <h3><b>Interacting with Customers</b></h3>
                            <p>Driver helpers communicate with customers and clients to ensure they’re happy with the
                                service they received. They also ask for feedback to resolve a customer’s complaints or
                                issues if necessary. </p>
                            <h3><b>Assisting with Navigation and Routes</b></h3>
                            <p>As part of this job, driver helpers work with the main office or Yard logistics staff to
                                update the driver of any route changes, assist with navigating to locations, and keep
                                track of schedules.</p>
                            <h3><b>Skills and Qualification</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Secondary Level Education (At least form 4)</li>
                                <li>HSE Qualification or Experience</li>
                                <li>2-3 Yeas relevant work experience with a Sino truck (HOWO) Abnormal load driving
                                    experience.</li>
                                <li>Valid international passport and yellow fever card</li>
                                <li>Class CE driving license with PSV</li>
                                <li>Recommendation letter from 2 civil servants</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "Transit";
                    $job_title = "Transit Controller (Zambia)";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Transit Controller (Zambia)</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Education, Skills & Qualifications</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>A degree holder in any relevant field</li>
                                <li>Strong interpersonal, attention to detail and deadline oriented </li>
                                <li>Comfortable with Microsoft Excel and accounting software</li>
                                <li>Have the ambition to help drive the growth of a growing company.</li>
                                <li>Strong communication and administration skills.</li>
                            </ul>
                            <br>
                            <h3><b>Responsibilites & Duties</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Preparation of tracking reports, accident, and all other related reports required
                                    internally or externally.</li>
                                <li>Preparations delivery notes </li>
                                <li>Management of Loading and offloading trucks (including interchanges for containers)
                                </li>
                                <li>Documentation and presentation of documents to clearing agents</li>
                                <li>Coaching drivers for continual improvement through proper communication and guidance
                                </li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "Tyre";
                    $job_title = "Tyre Supervisor";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Tyre Supervisor</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Responsibilites & Duties</b></h3>
                            <p>Supervises and coordinates activities of workers engaged in servicing and repairing
                                automobile and truck tires and tubes: Examines damaged, defective, or flat tires to
                                determine feasibility of repair and assigns workers to tasks, such as removing nails,
                                changing tubes, installing boots, and changing tires on vehicles.</p>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Maintains and orders tire installation equipment. Responsible for the safe and
                                    proper operation of tire bay equipment.</li>
                                <li>Maintains a clean and professional tire bay and waiting area. Merchandises tire
                                    displays and automotive products. </li>
                                <li>Complies with safety and cleanliness standards. Manages tire bay checklists.</li>
                                <li>Supervises tire bay team members. Ensures that team members work in a safe
                                    environment and are following Vikram Logistics QHSE policies and procedures.</li>
                                <li>performs other duties as assigned, including working in other departments as needed
                                </li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#{{$job_application_id}}" data-toggle="modal"> <a class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                        $job_application_id = "tracking";
                        $job_title = "Tracking Controller";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Tracking Controller</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Duties</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Responsible for monitoring and recording truck location and status.</li>
                                <li>Working as part of a team in the tracking call centre and function as a vehicle
                                    tracking operator and knowledge in Transport and logistics support </li>
                                <li>-Tracking of the fleet and insure the trucks have covered the the targeted kms</li>
                                <li>Computer Literacy to effectively operate and manage all the online systems</li>
                                <li>Communicate any issue that prevents the efficient transit of goods or any other
                                    emergency related issues to the senior</li>
                                <li>A minimum of Certificate or Diploma in any relevant discipline or equivalent
                                    vocational certificate. </li>
                                <li>Fluent English and Kiswahili.</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "Workshop";
                    $job_title = "Workshop Manager";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Workshop Manager</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Purpose</b></h3>
                            <p>This position will manage and lead a team of people in allocating workload and managing
                                the day to day operations in the workshop. It requires the job holder to co-ordinate
                                resources and liaise with internal and external key contacts to ensure work is delivered
                                on time and to a quality standard. </p>
                            <h3><b>Responsibilites & Duties</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Responsible for job allocation and job flow in the workshop including job scheduling
                                    and labour allocation for day to day operations. </li>
                                <li>Provide feedback to staff that are not performing to the expected level and ensure
                                    human resources are informed of trends in performance.</li>
                                <li>Assist with quoting, project management, materials purchasing and quality checks.
                                </li>
                                <li>Ensure supplies are ordered for each job. </li>
                                <li>Regularly report progress on each job and quickly communicate delays or concerns
                                    with the Director – Operations</li>
                                <li>Develop and implement systems to record, file and store information pertaining to
                                    client enquiries.</li>
                                <li>Ensuring all assets and equipments are available all time.</li>
                                <li>Identify staff that require on-going training and implement training opportunities
                                    to ensure their skills are improved. Work with apprentices and semi-skilled staff to
                                    role model appropriate technical skills</li>
                                <li>Act as a technical adviser on key projects and other areas of the business as
                                    requested.</li>
                                <li>Promote a Health and Safety culture within the business. </li>
                            </ul>
                            <h3><b>Qualifications & Skills</b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Diploma or Degree in Mechanical engineering preferred, but not mandatory.</li>
                                <li>Strong interpersonal skills </li>
                                <li>Ability to work with people at various levels from shop floor to senior management
                                </li>
                                <li>People management skills</li>
                                <li>Project management experience</li>
                                <li>Computer literacy with Microsoft Office including Outlook, Word and Excel and online
                                    fleet management and tracking system.</li>
                                <li>Can look beyond the initial customer enquiry and identify other business
                                    opportunities </li>
                                <li>Over 3 years of experience in workshop operations, of trucking and logistics.</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "Workshop_m";
                    $job_title = "Workshop Managers ";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Workshop Managers </h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Responsibilites & Duties </b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Should be responsible for the general servicing, maintenance, repair of the vehicles
                                    and investigating causes of breakdowns if any.</li>
                                <li>Should have 5 + years of experience in Transport and Logistics service support and
                                    experience in all type of vehicle brands.</li>
                                <li>Should be Proficient in Microsoft Office software including Excel, Word, and
                                    PowerPoint</li>
                                <li>Should Maintain schedules of the workshop hours on day to day bases.</li>
                                <li>Should organize and supervise the servicing, maintenance and repair of the company’s
                                    fleet and ensure that the vehicles are in good working conditions at all times.</li>
                                <li>Shall ensure that the vehicles come to the workshop at the designated kilometres for
                                    servicing.</li>
                                <li>Shall investigate and diagnose the causes of vehicle breakdown and accidents and
                                    apportion blames accordingly by ensuring that defaulters take financial
                                    responsibility of the cost of repairs.</li>
                                <li>Should ensure that the workshop is functional at all times and that vehicles
                                    awaiting repairs are attended to with dispatch.</li>
                                <li>Should provide purchasing manager periodically with the list of parts that are
                                    frequently used for replacement in order to avoid stock outs.</li>
                                <li>Vehicles for maintenance must be assigned to a designated Mechanic(s) and proper
                                    note taken in case the vehicles fails.</li>
                                <li>Should Design a working time sheet to ensure vehicles do not exceed the required
                                    time once inside workshop. At least 24hours time the truck should be out of
                                    workshop.</li>
                                <li>Should ensure the mobile maintenance team/Vehicle is always available when called
                                    upon. </li>
                                <li>Recover to base all broken down vehicles or accidental trucks/trailers with
                                    dispatch.</li>
                                <li>Ensure that the truck/trailer are thoroughly checked and bolt and nuts retightened
                                    on the arrival of the vehicles.</li>
                                <li>Should organize training for the workshop team and helpers to improve the quality of
                                    service.</li>
                                <li>Shall coordinate the activities of Mechanics and instil discipline on them.</li>
                                <li>Ensure that all the tools and equipment in the workshop are functional and are being
                                    prudently used.</li>
                                <li>Shall organize training in conjunction with the training unit for all the drivers
                                    especially on how to handle minor break downs.</li>
                                <li>Must avoid pilfering of the company’s tools and materials.</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "aTyre_m";
                    $job_title = "Tyre Manager Position & Duty";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Tyre Manager Position & Duty</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Responsibilites & Duties </b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Wheel alignments/post-alignment checks and road testing where essential.</li>
                                <li>Identify vehicle faults that may contribute to tyre wear and tare.</li>
                                <li>Ensure the order and maintenance of tyres are done in professional way</li>
                                <li>Liaising with key staff including workshop foreman to ensure efficient vehicle
                                    completion times.</li>
                                <li>Should Hold appropriate trade qualifications - motor mechanic certificatE</li>
                                <li>Should have not less than 5 years of previous experience in tyre management.</li>
                                <li>Possess basic computer competencies</li>
                                <li>Have experience in stock list management</li>
                                <li>Be reliable, honest, punctual, hard-working, self-motivated and able to work
                                    unsupervised.</li>
                                <li>Ensure the vehicles are checked accurately while entering the premises</li>
                                <li>Should be an Opportunity to be become part of a supportive and professional team.
                                </li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#apply-job" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                    @php
                    $job_application_id = "Health";
                    $job_title = "Health & Safety Advisor";
                    @endphp
                    @include('layouts.job-application-form')
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <h4>Health & Safety Advisor</h4>
                        </div>
                        <div class="accord-content">
                            <h3><b>Responsibilites & Duties </b></h3>
                            <ul style="list-style: circle;padding-left: 20px;">
                                <li>Should have Qualifications and/or greater than 5 years’ experience in fields.</li>
                                <li>Should be Established in understanding the Workplace Health and Safety legislation.
                                    (E.g. Osha Rules and Regulations)</li>
                                <li>Excellent written and verbal communication skills</li>
                                <li>Experience in the use of Microsoft and Online Reporting Platforms</li>
                                <li>Demonstrated ability to work both independently and in team settings</li>
                                <li>Should have Skill to advise the organisation with regard to occupational hygiene
                                    matters</li>
                                <li>Experience in the delivery of training to staff at various levels</li>
                                <li>Should have ability to Control, Train and Maintain the HSE Policy</li>
                                <li>Records, reports and analyses HSE performance metrics including both leading and
                                    lagging indicators. Delivers periodic performance reports for trends and conditions
                                    and alerts line management to problems.</li>
                                <li>Implements programmes to continuously improve HSE performance in the logistics
                                    business.</li>
                                <li>Identifies common HSE hazards inherent in the Assignment's operations and implements
                                    guidelines for protection from those hazards.</li>
                                <li>Conduct periodic audits to provide assurance of, or identify deficiencies in, HSE
                                    management within Assignment and record all reportable issues within agreed action
                                    tracking systems.</li>
                                <li>High level of communication skills in both written and spoken format</li>
                                <li>Should able to Provide advice and assistance to managers and supervisors on HSE
                                    management.</li>
                                <li>Liaise with client representatives and external bodies to identify safety
                                    requirements and standards</li>
                                <li>Review, improve and implement procedures for HSE management and Complete safety
                                    investigations and reporting.</li>
                                <li>Harnessing technology to facilitate improved performance in HSE</li>
                                <li>Continuing to develop a personalised brand of safety within our team</li>
                            </ul>
                            <br>
                            <div class="more-about" data-target="#{{$job_application_id}}" data-toggle="modal"> <a
                                    class="btn btn-primary btn-lg" href="javascript:void(0)">Apply For Job <i
                                        class="fa fa-chevron-circle-right"></i></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection