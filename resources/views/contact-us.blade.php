@extends('layouts.app')

@section('content')

<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>any question</h3>                        
                    <h2>Contact  Us</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>                        
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= --> 

<!-- =-=-=-=-=-=-= Google Map =-=-=-=-=-=-= -->
<section id="google-map">
        <div class="container-fluid no-padding">
            <div id="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.612517994704!2d39.28157381477236!3d-6.816893095073123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x185c4b38d82808d5%3A0xf2d1fc9eae815efd!2sVikram+Logistics+Tanzania+Limited!5e0!3m2!1sen!2stz!4v1555478396531!5m2!1sen!2stz" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div><!-- container-fluid end -->
</section>
<!-- =-=-=-=-=-=-= Google Map End =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= Contact Us =-=-=-=-=-=-= -->
<section id="contact-us" class="section-padding-70">
        <div class="container"> 
          <!-- Row -->
          <div class="row">            
          <div class="col-md-6">
            <div class="notice success" id="success"><p>Thanks so much for your message. We check e-mail frequently and will try our best to respond to your inquiry.</p></div>
            
            <form id="contactForm"  method="post"  action="{{url('contact-us')}}">
              {{ csrf_field() }}
               <div class="col-sm-6">
                                <!-- Name -->
                  <div class="form-group">
                    <label>Name<span class="required">*</span></label>
                    <input type="text" placeholder="Name" id="name" name="name" class="form-control inputs" required>
                  </div>
                </div> <!-- End col-sm-6 -->
              
                <div class="col-sm-6">
                  <!-- Email -->
                  <div class="form-group">
                    <label for="email">Email<span class="required">*</span></label>
                    <input type="email" placeholder="Email" id="email" name="email" class="form-control inputs" required>
                  </div>
                </div> <!-- End col-sm-6 -->
                
                
                <div class="col-sm-6">
                  <!-- Email -->
                  <div class="form-group">
                    <label>Subject<span class="required">*</span></label>
                      <input type="text" placeholder="Subject" id="subject" name="subject" class="form-control inputs" required>
                  </div>
                </div> <!-- End col-sm-12 -->

                <div class="col-sm-6">
                  <!-- Phone Number -->
                  <div class="form-group">
                      <label>Phone Number<span class="required">*</span></label>
                      <input type="text" placeholder="Phone Number" id="phonenumber" name="phone_number" class="form-control inputs" required>
                  </div>
                  </div> <!-- End col-sm-6 -->
                

                <div class="col-sm-12">
                  <!-- Comment -->
                  <div class="form-group">
                    <label>Message<span class="required">*</span></label>
                    <textarea placeholder="Message..." id="message" name="message"  class="form-control inputs" rows="6" required></textarea>
                  </div>
                </div> <!-- End col-sm-12 -->

                <div class="col-sm-12 text-cent er">
                  <div class="g-recaptcha" data-sitekey="{{config('recaptcha.site_key')}}"></div> 
                </div> <!-- End col-sm-6 --> <br>

                <div class="col-sm-12 text-cent mt-1 er" style="margin-top:20px;"> 
                  <button type="submit" id="yes" class="btn btn-primary">Send Message</button>
                    <img id="loader" alt="" src="images/loader.gif" class="loader">
                </div> <!-- End col-sm-6 -->
                              
               </form>               
                              
            </div>

            <div class="col-md-6" style="display:flex; flex-direction:column; margin-top:30px;">
              <div class="col-md-12">
                <div class="location-item text-center">
                  <div class="icon"> <i class="icon-map icon-icon"></i> </div>
                  <h4 class="text-uppercase">Location</h4>
                  <p> Jamhuri Street, Urban Rose Hotel, Mazzanine Floor.</p>
                </div>
              </div>
              <div class="col-md-12">
                <div class="location-item text-center">
                  <div class="icon"> <i class="icon-phone icon-icon"></i> </div>
                  <h4 class="text-uppercase">Call us 24/7</h4>
                  <p> +255 715 439 843 </p><br>
                </div>
              </div>
              <div class="col-md-12">
                <div class="location-item text-center">
                  <div class="icon"> <i class="icon-envelope icon-icon"></i> </div>
                  <h4 class="text-uppercase">Mail us</h4>
                    <p> Md@vikramlogistics.co.tz </p><br>
                </div>
              </div>
            </div>
            
             <div class="clearfix"></div>
          </div>
          <!-- Row End --> 
        </div>
        <!-- end container --> 
      </section>
@endsection