@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <div class="col-sm-12 col-md-6 page-heading text-left">
                        <h3>Think Safety First</h3>
                        <h2>QHSE</h2>
                    </div>
                    <div class="col-sm-12 col-md-6 text-right">
                        <ul class="breadcrumbs">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="#">QHSE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= About Section =-=-=-=-=-=-= -->

    <section class="padding-top-70" id="about" style="padding-bottom: 30px;">
        <div class="container">
            <div class="row clearfix">
                <!--Column-->
                <div class="col-md-9 col-sm-9 col-xs-9 ">
                    <div class="about-title">
                        <h2 style="font-size: 20px;">Quality, health, safety, environment</h2>
                        <p>Vikram logistics takes health and safety at the highest priority to protect our people and environment from harm and damage. We are a company that pride on providing high standards of health and safety precautions, accident prevention all incompliance to our international standards QHSE regulation. With a conception vision of being leading and pioneer company in our industry. Our in house HSE regulations provides training to all the driver ensuring work is done safe.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <div class="about-title">
                        <h2 style="font-size: 20px;"></h2>
                    <img src="images/osha.png" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- =-=-=-=-=-=-= About End =-=-=-=-=-=-= -->

    <section class="section-padding-70 gray" id="why-choose">
        <div class="container">
            <div class="row clearfix">
                <!--Column-->
                <div class="col-md-5 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        {{-- <h3>More about us</h3> --}}
                        <h2 style="font-size: 20px;">COMPANY ETHICS & ANTI BRIBEY </h2>
                        <p>We have a zero-tolerance policy to bribery and corruption. At Alistair Group we are committed to conducting business with the highest levels of integrity and in full compliance with international and local regulations, whatever the cost.</p>
                    </div>
                    
                </div>

                <div class="col-md-3 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        {{-- <h3>More about us</h3> --}}
                        <h2 style="font-size: 20px;">CORE VALUE AND ETHICS </h2>
                        <p></p>
                        <ul style="list-style: circle;padding-left: 20px;">
                            <li>Openness and Honesty</li>
                            <li>Trust and respect</li>
                            <li>Companion</li>
                            <li>Integrity</li>
                            <li>Customer focus</li>
                        </ul>
                    </div>
                    
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        {{-- <h3>More about us</h3> --}}
                        <h2 style="font-size: 20px;">OUR TEAM </h2>
                        <p></p>
                        <ul style="list-style: circle;padding-left: 20px;">
                            <li>CEO - Kalpesh Mehta</li>
                            <li>Managing Director - Vishal Mehta</li>
                            <li>Finance Manager - Mohammed Kara</li>
                            <li>Compliance Manager – Mavalla Msuya</li>
                            <li>Head Of Operations - Ezekiel Mestiack</li>
                            <li>Head of Human Resources - Veronica George</li>
                            <li>Procurement Manager - Emmanuel Boaz</li>
                        </ul>
                    </div>
                </div>

                <!-- Quote Form -->
                

                
                
            </div>
        </div>
    </section>



@endsection