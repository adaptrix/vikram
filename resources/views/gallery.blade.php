@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-6 page-heading text-left">
                    <h3>Our memories </h3>
                    <h2>Gallery Page</h2>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">Gallery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->


<!-- =-=-=-=-=-=-= Gallery =-=-=-=-=-=-= -->
<section id="gallery" class="custom-padding">
    <div class="container">

        <div class="portfolio-container text-center">


            <ul id="portfolio-grid" class="three-column hover-two">
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/1.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/1.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/2.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/2.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/3.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/3.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/4.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/4.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/5.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/5.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/1.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/6.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/7.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/7.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/8.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/8.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/9.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/9.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/10.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/10.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/11.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/11.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/12.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/12.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/13.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/13.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/14.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/14.jpg" alt="">
                        </a>
                    </div>
                </li>
                <li class="portfolio-item gutter">
                    <div class="portfolio">
                        <a class="tt-lightbox" href="images/portfolio/15.jpg">
                            <div class="tt-overlay"></div>
                            <img src="images/portfolio/15.jpg" alt="">
                        </a>
                    </div>
                </li>

                
            </ul>
        </div>

        <!-- portfolio-container -->



    </div>
    <!-- end container -->
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->
@endsection