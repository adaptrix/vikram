@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <div class="col-sm-12 col-md-6 page-heading text-left">
                        <h3>Who we are</h3>
                        <h2>About Our Company</h2>
                    </div>
                    <div class="col-sm-12 col-md-6 text-right">
                        <ul class="breadcrumbs">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="#">About Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= About Section =-=-=-=-=-=-= -->

    <section class="padding-top-70" id="about">
        <div class="container">
            <div class="row clearfix">
                <!--Column-->
                <div class="col-md-7 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        <h2>We Provide Services All Over The World</h2>
                        <p>Vikram Logistics Tanzania Limited ( herein referred as “VLT”) is young and dynamic company with experienced management team which aims to become a leading integrated global logistics and supply chain management company providing business enabling solutions and exceeding customer expectations. The company value on innovative logistic approach with professional work ethics.</p>
                        <p>VLT Services include port and custom clearance services, container storage services and transportation within Tanzania and neighboring countries including Kenya, Uganda, Rwanda, Burundi, Democratic Republic of Congo, Zambia, and Malawi. We believe to be as “one stop logistics service provider” for our customers.</p>
                        <p>VLT is amongst the logistic companies running a big transport fleet to curb the logistic demand for ready market. To reach to this success, major efforts have been appreciated from  our Directors and Employees whose wealth of experience, knowledge and pioneering spirit has been the corner stone for the company’s growth.</p>    
                        <p>Safety is, and always will be, at the top of our priority list. It has been proven throughout the years that successful transport companies credit safety as one of the key factors in the company’s longevity.</p>

                    </div>

                </div>

                <!-- RIght Grid Form -->
                <div class="col-md-5 col-sm-12 col-xs-12 our-gallery">
                    {{-- <a class="tt-lightbox" href="images/about-company-1.png"> <img class="img-responsive margin-bottom-30" alt="Image" src="images/about-company-1.png"></a>
                    <a class="tt-lightbox" href="images/about-company-2.png"> <img class="img-responsive margin-bottom-30" alt="Image" src="images/about-company-2.png"></a> --}}
                </div>
            </div>
        </div>
    </section>
    
    <!-- =-=-=-=-=-=-= About End =-=-=-=-=-=-= -->

    <section class="section-padding-70 gray" id="why-choose">
        <div class="container">
            <div class="row clearfix">

                <!--Column-->
                <div class="col-md-7 col-sm-12 col-xs-12 ">
                    <div class="about-title">
                        {{-- <h3>More about us</h3> --}}
                        <h2>Why Vikram Logistics</h2>
                        <p>At VLT, we believe the customer comes first; we are focused to ensure customer delight, superior quality of delivery and increased customer profitability.</p>
                    </div>
                    <div class="choose-services">
                        <ul class="choose-list">

                            <!-- feature -->
                            <li>
                                <div class="choose-box"> <span class="iconbox"><i class="flaticon-logistics-delivery-truck-and-clock"></i></span>
                                    <div class="choose-box-content">
                                        <h4>Fast Worldwide delivery</h4>
                                        <p>At VLT, at our operations all over the country, we practice an enduring value system based on an open culture, honest and fair business and personal conduct, earning the confidence and trust of our Associates and Customers.</p>
                                    </div>
                                </div>
                            </li>

                            <!-- feature -->
                            <li>
                                <div class="choose-box"> <span class="iconbox"><i class="flaticon-call-center-worker-with-headset"></i></span>
                                    <div class="choose-box-content">
                                        <h4>Safety & Compliance</h4>
                                        <p>At VLT, we practice transparency with all agencies that we are involved with. We value the importance of our colleagues, evolving a sense of togetherness and passion to deliver.
                                                </p>
                                    </div>
                                </div>
                            </li>

                            <li>
                                    <div class="choose-box"> <span class="iconbox"><i class="flaticon-person-standing-beside-a-delivery-box"></i></span>
                                        <div class="choose-box-content">
                                            <h4>Quality Of Delivery</h4>
                                            <p>At VLT, we believe the customer comes first; we are focused to ensure customer delight, superior quality of delivery and increased customer profitability.
                                            </p>
                                        </div>
                                    </div>
                                </li>
                        </ul>
                        <!-- end choose-list -->
                    </div>
                </div>

                <!-- Quote Form -->
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="accordion-box style-one">
                        <!-- Accordion -->
                        <div class="choose-services">
                                <ul class="choose-list">
        
                                    <!-- feature -->
                                <li>
                                    <div class="choose-box"> <span class="iconbox"><i class="flaticon-delivery-transportation-machine"></i></span>
                                        <div class="choose-box-content">
                                            <h4>Our Mission</h4>
                                            <p>From conception our mission has stayed true to be a market leader and innovator, within the logistics sector in Tanzania and subsequently the globe.</p>
                                        </div>
                                    </div>
                                </li>
    
                                <!-- feature -->
                                <li>
                                    <div class="choose-box"> <span class="iconbox"><i class="flaticon-delivery-truck"></i></span>
                                        <div class="choose-box-content">
                                            <h4>Our Vision</h4>
                                            <p>Has always been to be a footprint adopts, an integral and best practice approach, with high efficiency and effectiveness to always achieve our goals and meet our deadlines with unprecedented quality.</p>
                                            <p>To deliver customized and unique logistics solutions globally ensuring time effectiveness and cost savings for customers in a responsible and sustainable manner with unprecedented quality of Services.</p>
                                        </div>
                                    </div>
                                </li>
                                </ul>
                                <!-- end choose-list -->
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </section>



@endsection