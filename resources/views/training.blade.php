@extends('layouts.app')

@section('content')

<!-- =-=-=-=-=-=-= PAGE BREADCRUMB =-=-=-=-=-=-= -->
<section class="breadcrumbs-area parallex">
    <div class="container">
        <div class="row">
            <div class="page-title">
                <div class="col-sm-12 col-md-9 page-heading text-left">
                    {{-- <h3>CSR statement </h3> --}}
                    <h2>Training & Development</h2>
                </div>
                <div class="col-sm-12 col-md-3 text-right">
                    <ul class="breadcrumbs">
                        <li><a href="{{url('/')}}">home</a></li>
                        <li><a href="#">Training & Development</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =-=-=-=-=-=-= PAGE BREADCRUMB END =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= About Section =-=-=-=-=-=-= -->

<section class="padding-top-70" id="about" style="padding-bottom: 30px;">
    <div class="container">
        
        <div class="row clearfix">
            <!--Column-->
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <div class="about-title">
                    {{-- <h2>CSR Statement</h2> --}}
                    <p>There are many training and development opportunities at Vikram Logistics Tanzania Limited and we believe that everyone has the capacity to learn and grow. </p>
                    <p>We work with our people to identify the training they need to do their job safely and to the best of their
                        ability, and we even give our people time off for job-related training. We conduct regular training sessions
                        for our employees which help them to update their knowledge in the field of logistics and it benefits us to
                        provide effective and timely solutions to our clients. </p>
                    <p>Furthermore, we are technological savvy and adopt the latest developments from the information technology sector and make our employees equipped with it. Through our training sessions, our employees get
                        self-motivated and work efficiently which enhance our company’s growth.</p>
                </div>

            </div>

            <!-- RIght Grid Form -->
            <div class="col-md-6 col-sm-12 our-gallery col-xs-12" style="padding: 25px;">
                <a class="tt-lightbox" href="images/crew.png"> <img class="img-responsive" alt="Image" src="images/training/1.jpg"></a>

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <a class="tt-lightbox" href="images/crew.png"> <img class="img-responsive" alt="Image" src="images/training/2.jpg"></a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <a class="tt-lightbox" href="images/crew.png"> <img class="img-responsive" alt="Image" src="images/training/3.jpg"></a>
            </div>
        </div>
    </div>
</section>

@endsection