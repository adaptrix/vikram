<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUsMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $name,
        $email,
        $subject,
        $message,
        $phone_number,
        $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->name = $request->name;
        $this->email = $request->email;
        $this->message = $request->message;
        $this->phone_number = $request->phone_number;
        $this->subject = $request->subject; 
        $this->request=$request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.contact-us')->with('request',$this->request);
    }
}
