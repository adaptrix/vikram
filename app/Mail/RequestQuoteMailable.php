<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestQuoteMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_name, 
        $company_name,
        $email,
        $phone_number,
        $location_from,
        $subject,
        $location_to,
        $cargo_description;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->contact_name = $request->contact_name;
        $this->company_name = $request->company_name;
        $this->email = $request->email;
        $this->phone_number = $request->phone_number;
        $this->location_from = $request->location_from;
        $this->location_to = $request->location_to;
        $this->cargo_description = $request->cargo_description;
        $this->subject = "Requesting Quote";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.request-quote');
    }
}
