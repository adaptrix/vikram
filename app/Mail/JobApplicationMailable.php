<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class jobApplicationMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name, 
        $last_name,
        $email,
        $phone_number, 
        $location,
        $address,
        $job_title,
        $attachments,
        $subject,
        $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->first_name = $request->first_name;
        $this->lastname = $request->last_name;
        $this->email = $request->email;
        $this->phone_number = $request->phone_number;
        $this->location = $request->location;
        $this->address = $request->address;
        $this->job_title = $request->job_title;
        $this->attachments = $request->attachments;
        $this->request = $request;
        $this->subject = "Job Application";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        $email = $this->view('mail.job-application');  
        foreach($this->request->file_attachments as $attachment){
            $email->attach($attachment->getRealPath(),
            [
                'as' => $attachment->getClientOriginalName(),
                'mime' => $attachment->getClientMimeType(),
            ]);
        }
        return $email;
    }
}
