<?php

namespace App\Helpers;

use GuzzleHttp\Client;
 
class CheckRecaptcha
{
    public $response;
    public $secret_key;

    public function __construct($response){
        $this->response = $response;
        $this->secret_key = config('recaptcha.secret_key');
    }   

    public function isValid()
    {
        $client = new Client;
        $result = $client->post("https://www.google.com/recaptcha/api/siteverify",[
            "form_params"=> [
                "secret"=> $this->secret_key,
                "response"=> $this->response
            ]
        ])->getBody()->getContents();
        $result = json_decode($result);
        $is_valid = $result->success;
        return $is_valid;
    }
}
