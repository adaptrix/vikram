<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CheckRecaptcha;
use App\Mail\RequestQuoteMailable;
use App\Mail\ContactUsMailable;
use App\Mail\JobApplicationMailable;

use Mail;


class FormController extends Controller
{
    public function ContactUs(Request $request)
    {
        if($request->has('g-recaptcha-response')){ 
            $check_recaptcha = new CheckRecaptcha(request('g-recaptcha-response'));
            if($check_recaptcha->isValid()){
                // Mail::to('sarfaraz@legendaryits.com')->send(new ContactUsMailable($request));
                Mail::to('mwakalingajohn@gmail.com')->send(new ContactUsMailable($request));
                return redirect()->back()->with('message','Message sent successfully, we\'ll get back to you soon!');
            }else{
                return redirect()->back()->with('error','Recaptcha unsuccessful');
            }
        }else{
            return redirect()->back()->with('error','Recaptcha unsuccessful');
        }
    }
    
    public function RequestQuote(Request $request)
    {
        if($request->has('g-recaptcha-response')){ 
            $check_recaptcha = new CheckRecaptcha(request('g-recaptcha-response'));
            if($check_recaptcha->isValid()){
                // Mail::to('sarfaraz@legendaryits.com')->send(new RequestQuoteMailable($request));
                Mail::to('mwakalingajohn@gmail.com')->send(new RequestQuoteMailable($request));
                return redirect()->back()->with('message','Quote requested successfully, we\'ll get back to you soon!'); 
            }else{
                return redirect()->back()->with('error','Recaptcha unsuccessful');
            }
        }else{
            return redirect()->back()->with('error','Recaptcha unsuccessful');
        }
    }

    public function JobApplication(Request $request)
    { 
        
        if($request->has('g-recaptcha-response')){ 
            $check_recaptcha = new CheckRecaptcha(request('g-recaptcha-response'));
            if($check_recaptcha->isValid()){
                // Mail::to('sarfaraz@legendaryits.com')->send(new JobApplicationMailable($request));
                Mail::to('mwakalingajohn@gmail.com')->send(new JobApplicationMailable($request));
                return redirect()->back()->with('message','Job application sent successfully, we\'ll get back to you soon!'); 
            }else{
                return redirect()->back()->with('error','Recaptcha unsuccessful');
            }
        }else{
            return redirect()->back()->with('error','Recaptcha unsuccessful');
        }
    }
}
